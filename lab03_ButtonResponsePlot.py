'''
@file lab03_ButtonResponsePlot.py
@brief File containing the PC user interface that initializes and plots the Nucleo Button Response
@details This code prompts the user to input "g", sends this input to the 
         nucleo, and waits for the nucleo to send back button rrponse data. 
         Once it is received, it plots the data using matplotlib.
@author Jacob Winkler
@date February 3, 2021
'''
import serial
import time
from matplotlib import pyplot
import csv

# initializes serial port to communicate with nucleo
ser = serial.Serial(port='COM5',baudrate=115200,timeout=1)


print('Hello good person!! This is the PC User Interface for ME 405 Lab 3! '
      'This code interacts with a Nucleo L476 using serial communication ' 
      'and records and plots the first order voltage response of the push '
      'button on the Nucleo. So, what are you waiting for? Type "g" below and ' 
      'then push the button to watch the magic happen!!!')

while True:
    # input variable which starts data collection
    Go = input('Type "g" to start nucleo data collection: ')
    if Go == 'g':
        ser.write(str(Go).encode('ascii'))
        break
    else:
        print('Invalid command. Only "g" can start data collection!')
        
while True:
    if ser.in_waiting != 0:
        # reads the data from the serial port and spearates it into a list of
        # 2 strings
        data = ser.readline().decode('ascii').split('  ')
        # reads the data from the serial port and spearates it into a list of
        # 2 lists with the elements as strings
        out = [m.strip("array('H', [])\n").split(', ') for m in data]
        # converts first list in 'out' to Pin Voltage values as floats
        pin_volt_data = [int(t)*(3.3/4095) for t in out[0]]
        # converts second list in 'out' to time values as floats
        time_data = [int(t)/1000 for t in out[1]]
                
        # Plotting Data
        pyplot.plot(time_data, pin_volt_data)
        pyplot.xlabel('Time (seconds)')
        pyplot.ylabel('Voltage Output (V)')
        pyplot.title("Pin 5 ADC Voltage Response") 
        
        # variable that creates CSV File
        filename = 'ADC Voltage Output '+time.strftime('%Y%m%d-%H%M%S')+'.csv'
        with open(filename, 'w', newline='') as csv_file: 
            # variable that uses the csv.writer method from CSV package to 
            # generate a 2-column CSV 
            write = csv.writer(csv_file)
            for i in range(len(time_data)):
                write.writerow([time_data[i], pin_volt_data[i]]) 
        break 
    
# closes serial port    
ser.close()
