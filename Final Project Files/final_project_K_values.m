%% ME 405 Final Project: Deriving Controller Gain Values
clc;
clear;
close all;
clear vars;

%% Constants

r_m = 60e-3;               % radius of lever arm [m]
l_r = 50e-3;               % length of push rod [m]
r_B = 10.5e-3;             % radius of ball [m]
r_G = 42e-3;               % Vertical Distance from U-joint to platform CG [m]
l_P = 110e-3;              % Horizontal Distance from U-joint to push-rod pivot [m]
r_P = 32.5e-3;             % Vertical Distance from U-joint to push-rod pivot [m]
r_C = 50e-3;               % Vertical Distance from U-joint to platform surface [m]
m_B = 30e-3;               % Mass of Ball [kg]
m_P = 400e-3;              % Mass of Platform [kg]
I_P = 1.88e-3;             % Moment of Inertia of Platform [kg*m^2]
I_B = (2/5)*m_B*r_B^2;     % Moment of Inertia of Ball [kg*m^2]
b = 10e-3;                 % Viscous friction at U-joint [N*m *(s/rad)]
g = 9.81;                  % acceleration due to gravity [m/s^2]

%% Creating state space model
% the below matrix terms were created when linearizing the lab 5 
% derivation. the linearization was done at the equilibrium point, assuming
% x_o = u_o = 0. 

A = -((m_B*r_B^2) + (m_B*r_B*r_C) + I_B)/r_B;
C = -((m_B*r_B^2) +  I_B)/r_B;
D = -((m_B*r_B^2) + (m_B*r_B*r_C) + I_B);
H = (I_B + I_P + (m_B*r_B^2) + (2*m_B*r_B*r_C) + (m_B*r_C^2) + (m_P*r_G^2));
I = A*D + C*H;
J = -(g*m_B*(r_B + r_C)^2) - g*m_P*r_G - g*m_B*r_B;
K = -g*m_B*r_B;

% State space matrices: 
% X_DOT = AA*X + BB*u
% Y = CC*X + DD*u
% X_DOT = [x_dot, theta_dot, x_doubledot, theta_doubledot]
% X = [x, theta, x_dot, theta_dot]
% u = [T_m]

AA = [   0          0        1    0;
         0          0        0    1;
    -g*m_B*D/I  (D*J+H*K)/I  0  D*b/I;
     g*m_B*C/I  (A*K-C*J)/I  0  -C*b/I];
  
BB = [      0;
            0;
     (D*l_P)/(I*r_m);
    -(C*l_P)/(I*r_m)];

%% Closed loop model
% As presented in lecture, for state-feedback the control input is defined 
% as u =-K*x, with K defined below. Plugging this into the state-space 
% equation x_dot = A*x +B*u results in the expression x_dot = (A-BK)*x. The
% matrix term A-BK can then be interpreted as a new AA_cl matrix for the 
% closed loop system.

syms K_1 K_2 K_3 K_4

K = [K_1 K_2 K_3 K_4];

syms S

sI = [S 0 0 0;
      0 S 0 0;
      0 0 S 0;
      0 0 0 S];

AA_cl = AA - BB*K;

% From the closed loop matrix we can find the characteristic polynomial 
% for the system under closed loop feedback. Recall that the characteristic
% polynomial can be computed as P_cl = det(sI - A) evaluated with the new 
% closed-loop matrix.

P = det(sI - AA_cl);

% At this point, the code was run and the coeficients of P (for each power 
% of s) were recorded into the matrix below.

P_coeff = [1;                                     % s^4
           32.4991*K_3 - 703.2272*K_4 + 3.8358;   % s^3
           32.4991*K_1 - 703.2272*K_2 - 54.3813;  % s^2
           -4927.6*K_3;                           % s
           -4927.6*K_1 - 791.0163];               % s^0
       
%% Desired system model
% inputing the parameters w_n and zeta of the system that we want, we can
% now determine the coefficients of the desired system characteristic 
% polynomial, P_des

w_n = 5;            % natural frequency [rad/s]
zeta = 0.5;         % damping ratio [-]

sys_des = tf([w_n^2], [1, 2*w_n*zeta, w_n^2]);

poles_sys = pole(sys_des);

% The remaining two pole locations, z1 and z2, are then selected to be at 
% least 10x larger than the real component of the two poles associated with
% the input w_n and zeta.

z1 = -50;
z2 = -54;

poles_des = [poles_sys;
             z1;
             z2];

% Now, find the coeffiecients for the polynomial representing all of these
% desired poles 

P_des = poly(poles_des)';

%% Solve for Gains using polynomial matching
% setting the coefficients of the actual system, P_coeff, equal to the 
% coeffiecients of the desired system, P_des, we can solve for the K
% values.

coeff_eqn = eq(P_coeff, P_des);

[K1 K2 K3 K4] = solve(coeff_eqn, [K_1 K_2 K_3 K_4]);
K_sol = [double(K1) double(K2) double(K3) double(K4)];

disp(['The required gain values to control the balancing platform system are:']);
disp(['K3 = ', num2str(K_sol(1)), ', K4 = ', num2str(K_sol(2)), ', K1 = ', num2str(K_sol(3)), ', and K2 = ', num2str(K_sol(4)),]);
disp(['where T = K3*x + K4*theta + K1*xdot + K2*thetadot']);