"""
@file projectFile.py

@brief File containing the tasks, methods, and source code required to balance a ball on the provided touchpanel plate from ME 405 
@details File to...

@author Rick Hall
@author Jacob Winkler
@date Sat Mar 13 14:22:21 2021
"""

import pyb
from micropython import const, alloc_emergency_exception_buf
import gc
import utime

import cotask
import task_share
# import print_task

import machine
from bno055 import BNO055

from TouchPanel import TouchPanel
from MotorDriver import MotorDriver
from EncoderDriver import EncoderDriver


# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)

  
def xTable ():
    '''
    @brief Run the X-axis controller
    '''
    # -----   Intiialize Hardware Here   -----

    
    # Input Controller Gains Here
    K1 = -0.05
    K2 = 0.0015
    K3 = -0.005
    K4 = 3.5
    # Note that K1 and K3 are scaled down by 1000 from the gains calculated in
    # MATLAB, since the touch panel reads in millimeters, but the gains are 
    # calculated with lengths in meters
    
    # Conversion from Torque to Duty Cycle: D = (100*R)/(V_dc*K_t)
    Conversion = 100*2.21/(12*0.0138) 
    
    deg2rad = 3.14/180
    
    Sat = 50
    stk = 30
    ded = 20
    
    slp = (Sat-stk)/100
    
    # -----   Run Task Here   -----    
    while True:
        if Motor_Fault.get() == 0:
            # Retrieve most recent sensor data
            if X.any() and ThY.any():
                x = X.get()
                thy = ThY.get() * deg2rad
                xd = X_dot.get()
                thyd = ThY_dot.get() * deg2rad
            
                # Compute required motor torque
                T = K3*x + K4*thy + K1*xd + K2*thyd
                
                # Convert torque to duty cycle
                d = Conversion * T / 4
                
                # Saturation and Nonlinearity to overcome stiction
                if -ded <= d <= ded:
                    d = 0
                elif ded < d <= Sat:
                    d = stk + slp*d
                elif -Sat <= d < -ded:
                    d = -stk + slp*d
                elif d < -Sat:
                    d = -Sat
                elif d > Sat:
                    d = Sat
                
                # Output signal to motor
                motY.set_duty(d)
                
                # print('dX: ' + str(d) + '\n' + 
                #       'tX: ' + str(thy) + ' , ' + str(thyd) + '\n' +
                #       'X: ' + str(x) + ' , ' + str(xd) + '\n')
        
        yield(0)

def yTable ():
    '''
    @brief Run the Y-axis controller
    '''
    # -----   Intiialize Hardware Here   -----

    
    # Input Controller Gains Here
    K1 = -0.05
    K2 = 0.0015
    K3 = -0.005
    K4 = 3.5
    # Note that K1 and K3 are scaled down by 1000 from the gains calculated in
    # MATLAB, since the touch panel reads in millimeters, but the gains are 
    # calculated with lengths in meters
    
    # Conversion from Torque to Duty Cycle: D = (100*R)/(V_dc*K_t)
    Conversion = 100*2.21/(12*0.0138)
    
    deg2rad = 3.14/180
    
    Sat = 50
    stk = 30
    ded = 20
    
    slp = (Sat-stk)/100
    
    # -----   Run Task Here   -----    
    while True:
        if Motor_Fault.get() == 0:
            # Retrieve most recent sensor data
            if Y.any() and ThX.any():
                y = Y.get()
                thx = ThX.get() * deg2rad
                yd = Y_dot.get()
                thxd = ThX_dot.get() * deg2rad
            
                # Compute required motor torque
                T = K3*y + K4*thx + K1*yd + K2*thxd
                
                # Convert torque to duty cycle
                d = Conversion * T / 4

                # Saturation and Nonlinearity to overcome stiction
                if -ded <= d <= ded:
                    d = 0
                elif ded < d <= Sat:
                    d = stk + slp*d
                elif -Sat <= d < -ded:
                    d = -stk + slp*d
                elif d < -Sat:
                    d = -Sat
                elif d > Sat:
                    d = Sat
                    
                # Output signal to motor
                motX.set_duty(d)
                
                # print('dY: ' + str(d) + '\n' + 
                #       'tY: ' + str(thx) + ' , ' + str(thxd) + '\n' + 
                #       'Y: ' + str(y) + ' , ' + str(yd) + '\n') 
                
        yield(0)  

def chkEnc ():
    '''
    @brief Read table angle orientation data from encoder
    '''
    # -----   Intiialize Hardware Here   -----
    
    ency = EncoderDriver('PB6','PB7',4)
    encx = EncoderDriver('PC6','PC7',8)
    print('Encoders enabled!')
    
    delta_time = 0
    
    # Conversion from degrees on the motor shaft to degrees tilt in the table
    Conv = 60/110
    
    T1 = utime.ticks_us()
    
    # -----   Run Task Here   -----    
    while True:
        T2 = utime.ticks_us()
        delta_time = utime.ticks_diff(T2,T1) 
        
        encx.update()
        ency.update()
        
        degX = Conv*encx.deg_pos
        degY = Conv*ency.deg_pos
        ddX = (encx.deg_delta)/(delta_time/1e6)
        ddY = (ency.deg_delta)/(delta_time/1e6)
        
        ThX.put(degX)
        ThY.put(degY)
        
        ThX_dot.put(ddX)
        ThY_dot.put(ddY)
        
        T1 = utime.ticks_us()
                
        yield(0)

def chkIMU ():
    '''
    @brief Read table angle orientation data from IMU
    '''
    # -----   Intiialize Hardware Here   -----
    
    i2c = machine.I2C(1)    
    imu = BNO055(i2c)
    calibrated = False
    print('IMU enabled!')
    
    gyro = 0
    head = 0

    # -----   Run Task Here   -----    
    while True:
        if not calibrated:
            calibrated = imu.calibrated()
        gyro = imu.gyro()
        head = imu.euler()
        
        ThX.put(head[1])
        ThX_dot.put(gyro[1])
        
        ThY.put(head[2] - 2)
        ThY_dot.put(gyro[0])
        
        yield(0)

def chkTch ():
    '''
    @brief Read ball position data from resistive touch panel
    '''
    # -----   Intiialize Hardware Here   -----
    
    tch = TouchPanel('PA0','PA6','PA1','PA7')
    print('Touchpanel enabled!')
    
    x_last = 0
    y_last = 0
    
    contact = 0
    
    delta_time = 0
    
    T1 = utime.ticks_us()
    
    # -----   Run Task Here   -----
    while True:

        if tch.touch == False:
            tch.z_read()
            contact = 0
        if tch.touch == True:
            tch.coord_read()
            contact = 1
        
        T2 = utime.ticks_us()
        delta_time = utime.ticks_diff(T2,T1)
        
        if contact == 1:
            X.put(tch.coord[0])
            Y.put(tch.coord[1])
            X_dot.put((tch.coord[0] - x_last)/(delta_time/1e6))
            Y_dot.put((tch.coord[1] - y_last)/(delta_time/1e6))
        elif contact == 0:
            X.put(0)
            Y.put(0)
            X_dot.put(0)
            Y_dot.put(0)
                  
        T1 = utime.ticks_us()
        x_last = tch.coord[0]
        y_last = tch.coord[1]
        
        yield(0)

def storeData ():
    '''
    @brief Save position data to CSV file
    '''
    with open ("TouchPosData.csv", "w") as csvFile:
        csvFile.write('Time ,  , X_Position , X_Velocity , Angle_Y , Angular_Velocity_Y ,  , Y_Position , Y_Velocity , Angle_X , Angular_Velocity_X\n')
        csvFile.write(' [s] ,  , [mm] , [mm/s] , [deg] , [deg/s] ,  , [mm] , [mm/s] , [deg] , [deg/s]\n')
        t0 = utime.ticks_us()
        print('Recording Data to CSV...')
        while True:
            if X.any() and ThX.any() and Y.any() and ThY.any():
                xpos = X.get()
                ypos = Y.get()
                xdot = X_dot.get()
                ydot = Y_dot.get()
                thx = ThX.get()
                thdx = ThX_dot.get()
                thy = ThY.get()
                thdy = ThY_dot.get()
                t1 = utime.ticks_us()
                tdif = utime.ticks_diff(t1,t0)
                
                line = '{t:.4f} ,  , {a1:.2f} , {b1:.2f} , {c1:.2f} , {d1:.2f} ,  , {a2:.2f} , {b2:.2f} , {c2:.2f} , {d2:.2f}\n'
                line = line.format(t=(tdif/1e6),a1=xpos,b1=xdot,c1=thy,d1=thdy,a2=ypos,b2=ydot,c2=thx,d2=thdx)
                csvFile.write(line)
                print('Data recorded: ' + line)
        
            yield(0)
        


#------------------------------------------------------------------------------
if __name__ == '__main__':
    
    
    
    # User Inputs Here:
        
    Rx = False     # Run X-Axis Controller
    Ry = True     # Run Y-Axis Controller
    
    EI = 1        # Use Enc (0) or IMU (1)
    
    CD = 1        # Collect data in CSV (No = 0, Yes =1)
    
    if EI == 0:
        input('Encoder tracking is enabled. \nPlease level the table, then press enter:     >>> ')
        
        
    '''
    List of shared variables:
        
        User Input Boolean Flags:
            
        - Run_Xaxis        (enable controller on X)    {share}
        - Run_Yaxis        (enable controller on Y)    {share}
        
        - Enc_IMU          (Toggle encoder/IMU angle)  {share}
                                - 0 = Enc, 1 = IMU
    
        - Collect_Data     (push data to CSV file?)    {share}
        
        System Boolean Flags:
            
        -Motor_Fault       (Disables motors if fault)  {share}
            
        State Varibles:
            
        - X        [x-position]           (from touch panel)        {queue}
        - X_dot    [x-velocity]           (from touch panel)        {queue}
        - ThY      [x-angle]              (from encoder or IMU)     {queue}
        - ThY_dot  [x-angular velocity]   (from encoder or IMU)     {queue}
        
        - Y        [y-position]           (from touch panel)        {queue}
        - Y_dot    [y-velocity]           (from touch panel)        {queue}
        - ThX      [y-angle]              (from encoder or IMU)     {queue}
        - ThX_dot  [y-angular velocity]   (from encoder or IMU)     {queue}
        
    '''    
    
    # Shared Variables Definitions:   
    Motor_Fault = task_share.Share('i', name='Motor_Fault')
    
    # Queue Definitions:
    X = task_share.Queue('f', 8, overwrite=True, name="X")
    X_dot = task_share.Queue('f', 8, overwrite=True, name="X_dot")
    ThY = task_share.Queue('f', 8, overwrite=True, name="ThY")
    ThY_dot = task_share.Queue('f', 8, overwrite=True, name="ThY_dot")
    
    Y = task_share.Queue('f', 8, overwrite=True, name="Y")
    Y_dot = task_share.Queue('f', 8, overwrite=True, name="Y_dot")
    ThX = task_share.Queue('f', 8, overwrite=True, name="ThX")
    ThX_dot = task_share.Queue('f', 8, overwrite=True, name="ThX_dot")


    # Initialize Motor Drivers:
    motX = MotorDriver('A15', 'B0', 'B1', 3, 2)
    motY = MotorDriver('A15', 'B4', 'B5', 3, 1)
    motX.enable()
    motY.enable()
    
    # ISR for motor fault:
    def fault_check(line):
        '''
        @brief This function calls when the nFault pin, pin B2, is low, indicating an overcurrrent condition in the motor.
        @param Line This param allows for functionality of the interrupt service request.
        '''
        # this code disables both motors and turns the fault variable to 1, 
        # triggering the motor fault user interface
        Motor_Fault.put(1)
        
    # Define external interupt to stop motors if there is a fault. The 
    # nFAULT pin, pin B2 on the motordriver chip, automatically detects motor 
    # faults (overcurrent conditions). It is active low, meaning that a low 
    # signal indicates a fault. 
    extint = pyb.ExtInt(pyb.Pin(pyb.Pin.board.PB2, mode=pyb.Pin.IN),
                        pyb.ExtInt.IRQ_FALLING, 
                        pyb.Pin.PULL_UP, 
                        callback = fault_check)
    
    # Initailly disable interupts so motors don't fault upon start
    Motor_Fault.put(0)
    extint.disable()
    utime.sleep_us(5)
    
    # Set up Tasks
    if Rx:
        task_1 = cotask.Task (xTable, name = 'X_Controller', priority = 6, 
                              period = 30)
        cotask.task_list.append (task_1)
    
    if Ry:
        task_2 = cotask.Task (yTable, name = 'Y_Controller', priority = 5, 
                              period = 30)
        cotask.task_list.append (task_2)
    
    if not EI:    
        task_3 = cotask.Task (chkEnc, name = 'Read_Encoders', priority = 3,
                              period = 5)
        cotask.task_list.append(task_3)
        
    if EI:
        task_4 = cotask.Task (chkIMU, name = 'Read_IMU', priority = 3,
                              period = 5)
        cotask.task_list.append(task_4)
    
    task_5 = cotask.Task (chkTch, name = 'Read_TouchPanel', priority = 4,
                          period = 10)
    cotask.task_list.append(task_5)
    
    if CD:
        task_6 = cotask.Task (storeData, name = 'Collect_Data', priority = 7,
                              period = 250)
        cotask.task_list.append(task_6)
    
    # Run garbage collector to clean up memory
    gc.collect ()
    
    # Run the scheduler with the chosen scheduling algorithm. Quit if any 
    # character is sent through the serial port
    input("Set up complete! Press enter to enable balancing table:     >>> ")
    extint.enable()
    while True:
        try:
            if Motor_Fault.get():
                extint.disable()
                flt = 0
                motX.disable()
                motY.disable()
                
                try:
                    while flt != 'Fault Cleared':
                        flt = input("Fault Detected! Please Clear fault and rebalance the table.\nEnter 'Fault Cleared' to reset:     >>> ")
                except KeyboardInterrupt:
                    break
                        
                motX.enable()
                motY.enable()
                extint.enable()
                for n in range(8):
                    X.put(0)
                    X_dot.put(0)
                    Y.put(0)
                    Y_dot.put(0)
                    ThX.put(0)
                    ThX_dot.put(0)
                    ThY.put(0)
                    ThY_dot.put(0)
                Motor_Fault.put(0)
                
            cotask.task_list.pri_sched ()
            
        except KeyboardInterrupt:
            extint.disable()
            motX.disable()
            motY.disable()
            print("That's all folks!! \n[Insert copyrighted music]")
            break

      
    
    
    
    
    
    
