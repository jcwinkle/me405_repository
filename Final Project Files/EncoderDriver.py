'''
@file EncoderDriver.py
@brief File defining a generic encoder class
@author Jacob Winkler
@author Rick Hall
@date March 4, 2021 
'''
import pyb
import utime
import array

class EncoderDriver:
    '''
    @brief Driver for optical encoders using the pyboard timer module.
    @details This class utilizes the built-in timer module to track the 
    position of a quadrature encoder. The timer itself tracks position relative 
    to the position of the encoder when it is initialized. To calibrate the 
    location of the encoder and accurately track the relative position, methods 
    are included for adding change-in-position to a relative position attribute, 
    corrected for any overflow that occurs in the timer.
    '''
    def __init__(self, IN1_pin, IN2_pin, timer,PPR=4000):
        '''
        @brief Creates an encoder object.
        @param In1_pin The 1st pyb.Pin object that is specific to the desired encoder. Input must be a string!!
        @param In2_pin The 2nd pyb.Pin object that is specific to the desired encoder. Input must be a string!!
        @param timer The timer specific to the physical encoder
        @param PPR Encoder Pulses per Revolution (PPR); used to compute position in degrees.
        '''
        tim = pyb.Timer(timer)
        tim.init(prescaler=0, period=0xFFFF)
        pin_IN1 = pyb.Pin(IN1_pin)
        pin_IN2 = pyb.Pin(IN2_pin)
        tim.channel(1, pin=pin_IN1, mode=pyb.Timer.ENC_AB)
        tim.channel(2, pin=pin_IN2, mode=pyb.Timer.ENC_AB)
        
        ## Position Buffer - Contains current and previous position of the encoder timer
        self.pos = array.array('I',[0, 0])
        
        ## Relative Position - Contains the current calibrated relative position of the encoder
        self.rel_pos = 0
        
        ## Degree Position - Contains the relative position of the encoder, in degrees. 
        # Encoder PPR must be correctly input to ensure accuracy.
        self.deg_pos = 0
        
        ## Encoder timer object
        self.tim = tim      
        
        ## Change in timer position - stored for later use.
        self.bad_delta = 0
        
        ## Change in encoder position - corrected for timer overflow. Used in relative position tracking.
        self.good_delta = 0
        
        ## Change in encoder position, in degrees. Stored for external use.
        self.deg_delta = 0
        
        ## PPR of the encoder. Defaults to 4000, must be correct to get accurate degree angles
        self.PPR = PPR
            
    def update(self):
        '''
        @brief Updates the encoder's position and records encoder's previous position
        @details Reads the encoder timer, then computes the location of the encoder, 
        correcting for timer overflow if necessary. Also computes the position 
        and change-in-position of the encoder in degrees. 
        '''   
        self.pos[-2] = self.pos[-1]
        self.pos[-1] = self.tim.counter()
        self.bad_delta = self.pos[-1] - self.pos[-2]
        
        self.rel_pos = self.rel_pos + self.get_delta()
        
        self.deg_pos = 360 * (self.rel_pos / self.PPR)
        self.deg_delta = 360 * (self.good_delta / self.PPR)
        
    def get_delta(self):
        '''
        @brief Computes the difference between the encoder's current position and previous position
        @details When an excessively large change in the encoder timer is detected, 
        it is assumed that the timer has overflowed. This method corrects for the 
        overflow and returns the accurate change-in-position.
        @return self.good_delta The corrected change-in-position of the encoder, in encoder pulses.
        '''
        if -0x7FFF < self.bad_delta < 0x7FFF:
            self.good_delta = self.bad_delta
        elif self.bad_delta < -0x7FFF:
            self.good_delta = self.bad_delta + 0xFFFF
        elif self.bad_delta > 0x7FFF:
            self.good_delta = self.bad_delta - 0xFFFF
        else:
            pass
        return int(self.good_delta)
        
    def get_position(self):
        '''
        @brief Prints the encoder's current position
        '''
        return int(self.deg_pos)
    
    def set_position(self, pos):
        '''
        @brief Sets the encoder's position to a given input
        '''
        self.rel_pos = pos
        return self.rel_pos
    
    def zero(self):
        '''
        @brief Zeros out the encoder
        '''
        self.rel_pos = 0
        return self.rel_pos

    
if __name__ == '__main__':
    encx = EncoderDriver('PB6','PB7',4)
    ency = EncoderDriver('PC6','PC7',8)
    
    while True:
        try:
            t1 = utime.ticks_us()
            encx.update()
            ency.update()
            t2 = utime.ticks_us()
            readtime = utime.ticks_diff(t2,t1)
            
            line = 'Encoder X: {px:.2f} --> {dx:.2f}     Encoder Y: {py:.2f} --> {dy:.2f}'
            
            print('\n'*50)
            print(line.format(px=encx.deg_pos,dx=encx.deg_delta,py=ency.deg_pos,dy=ency.deg_delta))
            print('Readtime: ' + str(readtime))
            utime.sleep_ms(500)
            
        except KeyboardInterrupt:
            break