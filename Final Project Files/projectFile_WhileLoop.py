# -*- coding: utf-8 -*-
"""


@author Rick Hall
@author Jacob Winkler

@date Sat Mar 13 14:22:21 2021

"""

import pyb
from micropython import const, alloc_emergency_exception_buf
import gc
import utime
import array

import machine
from bno055 import BNO055

from TouchPanel import TouchPanel
from MotorDriver import MotorDriver
from EncoderDriver import EncoderDriver


# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)




# User Inputs Here:
    
Rx = True     # Run X-Axis Controller
Ry = True     # Run Y-Axis Controller

EI = 1        # Use Enc (0) or IMU (1)

CD = 0        # Collect data in CSV (No = 0, Yes =1)

if EI == 0:
    input('Encoder tracking is enabled. \nPlease level the table, then press enter:     >>> ')
    
# Input Controller Gains Here
K1 = 0.0033         
K2 = -0.27
K3 = 0.0091
K4 = -1
# Note that K1 and K3 are scaled down by 1000 from the gains calculated in
# MATLAB, since the touch panel reads in millimeters, but the gains are 
# calculated with lengths in meters

# Conversion from Torque to Duty Cycle: D = (100*R)/(V_dc*K_t)
Conversion = 100*2.21/(12*0.0138) 

deg2rad = 3.14/180

Sat = 80
stk = 30
ded = 10

slp = (Sat-stk)/100


'''
List of shared variables:
    
    User Input Boolean Flags:
        
    - Run_Xaxis        (enable controller on X)    {share}
    - Run_Yaxis        (enable controller on Y)    {share}
    
    - Enc_IMU          (Toggle encoder/IMU angle)  {share}
                            - 0 = Enc, 1 = IMU

    - Collect_Data     (push data to CSV file?)    {share}
    
    System Boolean Flags:
        
    -Motor_Fault       (Disables motors if fault)  {share}
        
    State Varibles:
        
    - X        [x-position]           (from touch panel)        {queue}
    - X_dot    [x-velocity]           (from touch panel)        {queue}
    - ThY      [x-angle]              (from encoder or IMU)     {queue}
    - ThY_dot  [x-angular velocity]   (from encoder or IMU)     {queue}
    
    - Y        [y-position]           (from touch panel)        {queue}
    - Y_dot    [y-velocity]           (from touch panel)        {queue}
    - ThX      [y-angle]              (from encoder or IMU)     {queue}
    - ThX_dot  [y-angular velocity]   (from encoder or IMU)     {queue}
    
'''    

# Shared Variables Definitions:   
Motor_Fault = 0

# Queue Definitions:
X = 0
X_dot = 0
ThY = 0
ThY_dot = 0

Y = 0
Y_dot = 0
ThX = 0
ThX_dot = 0


# Initialize Motor Drivers:
motX = MotorDriver('A15', 'B0', 'B1', 3, 2)
motY = MotorDriver('A15', 'B4', 'B5', 3, 1)
motX.enable()
motY.enable()

# Intitialize Touch Panel
tch = TouchPanel('PA0','PA6','PA1','PA7')

x_last = 0
y_last = 0

contact = 0

delta_time = 0

T1 = utime.ticks_us()

# Initialize IMU
i2c = machine.I2C(1)    
imu = BNO055(i2c)
calibrated = False

gyro = 0
head = 0

# Initialize Encoder
ency = EncoderDriver('PB6','PB7',4)
encx = EncoderDriver('PC6','PC7',8)

delta_time = 0

# Conversion from degrees on the motor shaft to degrees tilt in the table
Conv = 60/110

T3 = utime.ticks_us()


# ISR for motor fault:
def fault_check(line):
    '''
    @brief This function calls when the nFault pin, pin B2, is low, indicating an overcurrrent condition in the motor.
    @param Line This param allows for functionality of the interrupt service request.
    '''
    # this code disables both motors and turns the fault variable to 1, 
    # triggering the motor fault user interface
    global Motor_Fault
    Motor_Fault = 1
    
# Define external interupt to stop motors if there is a fault. The 
# nFAULT pin, pin B2 on the motordriver chip, automatically detects motor 
# faults (overcurrent conditions). It is active low, meaning that a low 
# signal indicates a fault. 
extint = pyb.ExtInt(pyb.Pin(pyb.Pin.board.PB2, mode=pyb.Pin.IN),
                    pyb.ExtInt.IRQ_FALLING, 
                    pyb.Pin.PULL_UP, 
                    callback = fault_check)

# Initailly disable interupts so motors don't fault upon start
extint.disable()
utime.sleep_us(5)

# Run garbage collector to clean up memory
gc.collect ()

# Run the scheduler with the chosen scheduling algorithm. Quit if any 
# character is sent through the serial port
input("Set up complete! Press enter to enable balancing table:     >>> ")
extint.enable()
while True:
    # try:
        
    # Touch Panel ------------    
    if tch.touch == False:
        tch.z_read()
        contact = 0
    if tch.touch == True:
        tch.coord_read()
        contact = 1
    
    T2 = utime.ticks_us()
    delta_time = utime.ticks_diff(T2,T1)
    
    if contact == 1:
        X = tch.coord[0]
        Y = tch.coord[1]
        X_dot = (tch.coord[0] - x_last)/(delta_time/1e6)
        Y_dot = (tch.coord[1] - y_last)/(delta_time/1e6)
    elif contact == 0:
        X = 0
        Y = 0
        X_dot = 0
        Y_dot = 0
              
    T1 = utime.ticks_us()
    x_last = tch.coord[0]
    y_last = tch.coord[1]
    
    # IMU --------------------------
    if EI:
        gyro = imu.gyro()
        head = imu.euler()
        
        ThX = head[1]
        ThX_dot = gyro[1]
        
        ThY = head[2] - 2
        ThY_dot = gyro[0]
    
    # Encoder ----------
    if not EI:
        T4 = utime.ticks_us()
        delta_time = utime.ticks_diff(T4,T3) 
        
        encx.update()
        ency.update()
        
        degX = Conv*encx.deg_pos
        degY = Conv*ency.deg_pos
        ddX = (encx.deg_delta)/(delta_time/1e6)
        ddY = (ency.deg_delta)/(delta_time/1e6)
        
        ThX = degX
        ThY = degY
        
        ThX_dot = ddX
        ThY_dot = ddY
        
        T3 = utime.ticks_us()
        
    # Motors
    # Retrieve most recent sensor data
    y = Y
    thx = ThX * deg2rad
    yd = Y_dot
    thxd = ThX_dot * deg2rad
    x = X
    thy = ThY * deg2rad
    xd = X_dot
    thyd = ThY_dot * deg2rad

    # Compute required motor torque
    Ty = K3*y + K4*thx + K1*yd + K2*thxd
    Tx = K3*x + K4*thy + K1*xd + K2*thyd
    
    # Convert torque to duty cycle
    dy = Conversion * Ty / 4
    dx = Conversion * Tx / 4

    # Saturation and Nonlinearity to overcome stiction
    if -ded <= dy <= ded:
        dy = 0
    elif ded < dy <= Sat:
        dy = stk + slp*dy
    elif -Sat <= dy < -ded:
        dy = -stk + slp*dy
    elif dy < -Sat:
        dy = -Sat
    elif dy > Sat:
        dy = Sat
        
    if -ded <= dx <= ded:
        dx = 0
    elif ded < dx <= Sat:
        dx = stk + slp*dx
    elif -Sat <= dx < -ded:
        dx = -stk + slp*dx
    elif dx < -Sat:
        dx = -Sat
    elif dx > Sat:
        dx = Sat
        
    # Output signal to motor
    motX.set_duty(dy)
    print('dY: ' + str(dx) + '\n' + 
          'tY: ' + str(thx) + ' , ' + str(thxd) + '\n' + 
          'Y: ' + str(y) + ' , ' + str(yd) + '\n')  
    
    # Output signal to motor
    motY.set_duty(dx)
    print('dX: ' + str(dy) + '\n' + 
          'tX: ' + str(thy) + ' , ' + str(thyd) + '\n' +
          'X: ' + str(x) + ' , ' + str(xd) + '\n')


        
    # except KeyboardInterrupt:
    #     extint.disable()
    #     motX.disable()
    #     motY.disable()
    #     print("That's all folks!! \n[Insert copyrighted music]")
    #     break

  






