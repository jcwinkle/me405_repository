'''
@file lab02_ReactionTimer.py
@brief File containing a reaction timer for ME 405 Lab 2
@details This file times and records reaction times using a nucleo L476RG. It 
         is meant to be run directly on the nucleo using an SSH terminal such 
         as PuTTY. The code sends a signal to the nucleo, turns on an LED after
         a random time between 2-3 seconds, waits for the user to press the 
         blue push-button on the nucleo, and uses a timer to record the time 
         from when the LED turned on to when the button was pushed. Then, it displays 
         the reaction time and repeats the process until the user types CTRL-C 
         to interrupt the program. Lastly, it calculates and displays the 
         average reaction time from all the recorded times from the most 
         recent run.
@author Jacob Winkler
@date January 27, 2021
'''

# Import any required modules
import pyb
import utime
import random
import micropython

# Enable interupt service requests
pyb.enable_irq

# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)

## Pin object to use for Blinking LED. Attached to PA5
LED = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

## Pin object to use for the Push Button. Attached to PC13
Button = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)
    
## Create a timer object (timer 2) in general purpose counting mode
# to count mircoseconds (Nucleo runs at 80MHz, so prescaler=79) 
tim2 = pyb.Timer(2, prescaler=79, period= 0x7FFFFFFF)

## List of reaction times, appended after each run
reac_times = [0]
n = 0

def button_push(line):
    '''
    @brief This function calls when the blue button on the Nucleo is pressed and records the reaction time.
    @param line This param allows for functionality of the interrupt service request.
    '''
    # Create an interrupt service routine
    if reac_times[n-1] == 0:
        reac_times[n-1] = tim2.counter()
    else:
        print('No! You must wait for the LED before pressing the button!!')
        
## Variable to initialize interupt
extint = pyb.ExtInt(Button,                  # Which pin
             pyb.ExtInt.IRQ_FALLING,         # Interrupt on falling edge
             pyb.Pin.PULL_UP,                # Activate pullup resistor
             callback=button_push)           # Interrupt service routine

    
# Main program loop that runs until User interupts program
while True:
    try:
        if len(reac_times) > n:
            n = len(reac_times)
            ## variable that generates a random integer between 1 and 1000
            var = random.randint(1, 1000)
            ## a random delay between 2-3 seconds generated from the 'random' module
            delay = (var/1000) +2
            utime.sleep(delay)
            LED.high()
            tim2.counter(0)
            utime.sleep(1)
            # prints the reaction time after the LED turns off
            print('Reaction: ' + str(reac_times[n-1]/1e6) + ' seconds')
            LED.low()
            
        if reac_times[-1] != 0:
            reac_times.append(0)
        
        
    except KeyboardInterrupt:
        # Keyboard interupt that stops the loop above recording reaction times.
        # Calculates and displays average reaction time
        
        ## Variable that calculates the average reaction time from all recorded
        # times in the list 'reac_times'
        avg_response = (sum(reac_times)/1E6)/(len(reac_times)-1)
        print('Your Average Response Time is ' +str(avg_response) +' seconds' )
        break
        
            
            