var files_dup =
[
    [ "EncoderDriver.py", "EncoderDriver_8py.html", "EncoderDriver_8py" ],
    [ "HW1_getChange.py", "HW1__getChange_8py.html", "HW1__getChange_8py" ],
    [ "lab01_Vendotron.py", "lab01__Vendotron_8py.html", "lab01__Vendotron_8py" ],
    [ "lab02_ReactionTimer.py", "lab02__ReactionTimer_8py.html", "lab02__ReactionTimer_8py" ],
    [ "lab03_ButtonResponsePlot.py", "lab03__ButtonResponsePlot_8py.html", "lab03__ButtonResponsePlot_8py" ],
    [ "lab03_NucleoMain.py", "lab03__NucleoMain_8py.html", "lab03__NucleoMain_8py" ],
    [ "lab04_MCU_temp_test.py", "lab04__MCU__temp__test_8py.html", "lab04__MCU__temp__test_8py" ],
    [ "lab04_NucleoMain.py", "lab04__NucleoMain_8py.html", "lab04__NucleoMain_8py" ],
    [ "lab04_TempDataPlot.py", "lab04__TempDataPlot_8py.html", "lab04__TempDataPlot_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "projectFile.py", "projectFile_8py.html", "projectFile_8py" ],
    [ "TouchPanel.py", "TouchPanel_8py.html", "TouchPanel_8py" ]
];