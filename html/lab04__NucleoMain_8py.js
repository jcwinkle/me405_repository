var lab04__NucleoMain_8py =
[
    [ "adcall", "lab04__NucleoMain_8py.html#ad20f4d43392b635cbedc8188ad578471", null ],
    [ "curr_time", "lab04__NucleoMain_8py.html#a0d002da61ac1e29cef1137de92e08bcf", null ],
    [ "filename", "lab04__NucleoMain_8py.html#abfa7d3629f3a8defa4886c77f87be10c", null ],
    [ "header", "lab04__NucleoMain_8py.html#a0d974d9d87e5663f14de7534d0397a96", null ],
    [ "mcp9808", "lab04__NucleoMain_8py.html#a3c4d6f71335c8179df8ce71fe1e74d1c", null ],
    [ "prev_time", "lab04__NucleoMain_8py.html#afb73a01f77fefaf83b5f98ce68104e91", null ],
    [ "start_time", "lab04__NucleoMain_8py.html#a9db72fa77b166b81f9fa4b2f6afd2756", null ],
    [ "temp_amb", "lab04__NucleoMain_8py.html#aa7f960e30b2fa15e3e0cb23f3d639f13", null ],
    [ "temp_data", "lab04__NucleoMain_8py.html#a3bb8b489f8a133fcd8044f50d9a9fa79", null ],
    [ "temp_int", "lab04__NucleoMain_8py.html#abb72aa05e397ac23fdae5f2b97fcb78a", null ],
    [ "time", "lab04__NucleoMain_8py.html#ae354058864da6f929181c6c8e19afbc9", null ],
    [ "time_diff", "lab04__NucleoMain_8py.html#a0632491dd275b67a5ee20e0ecb1d2976", null ],
    [ "time_s", "lab04__NucleoMain_8py.html#a7c8cab17582397d380d04b1c62541d4d", null ],
    [ "total_hrs", "lab04__NucleoMain_8py.html#a76bf3617313c4c35f61cdb4c1fc11de3", null ],
    [ "total_secs", "lab04__NucleoMain_8py.html#a2ae6dfbab82aa61a55faad2e2d9403a4", null ],
    [ "total_time", "lab04__NucleoMain_8py.html#ac6cac225934eac513a9bc0b2c85be9eb", null ]
];