var Lab4Nuke__Kai_8py =
[
    [ "adc", "Lab4Nuke__Kai_8py.html#a4894c732e67750d0ad8a1adb213b64f9", null ],
    [ "data", "Lab4Nuke__Kai_8py.html#ae5a72d847a6bda71e7d63361c54148c0", null ],
    [ "filename", "Lab4Nuke__Kai_8py.html#a996d81fcefa4f24f3284783a2050e8f0", null ],
    [ "header", "Lab4Nuke__Kai_8py.html#ab76f655058927b62190b3a5664803f3d", null ],
    [ "hrs", "Lab4Nuke__Kai_8py.html#ac3439291f33aa95b7366eb82e566830d", null ],
    [ "MCP9808", "Lab4Nuke__Kai_8py.html#a5fa2ee5f1cc5eb3ea7ebf9f1c9e65df1", null ],
    [ "t0", "Lab4Nuke__Kai_8py.html#a220ddd97b700f515bf6e5c9fa1b71f99", null ],
    [ "t_current", "Lab4Nuke__Kai_8py.html#aa195c733169916af8ecf88294988db7e", null ],
    [ "t_iter", "Lab4Nuke__Kai_8py.html#aacbe3f5985fb6a6bfc6fddc3764fd891", null ],
    [ "temp", "Lab4Nuke__Kai_8py.html#a0e15537a18ec1819250559fdd6175018", null ],
    [ "temp_A", "Lab4Nuke__Kai_8py.html#aa133f20a108a16f6c6ac546fb10186ad", null ],
    [ "ticks_0", "Lab4Nuke__Kai_8py.html#af97b8751f223857432b812fd198515e7", null ],
    [ "time_ms_run", "Lab4Nuke__Kai_8py.html#ad0b9b8977aa367ab5c0dd7193533ebbe", null ],
    [ "time_s", "Lab4Nuke__Kai_8py.html#a939788f4612d3f950ba59072d9825e79", null ],
    [ "total_time", "Lab4Nuke__Kai_8py.html#a320583d49c7f202448f86954b9e2e158", null ]
];