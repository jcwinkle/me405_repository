var classclosedloop_1_1closedloop =
[
    [ "__init__", "classclosedloop_1_1closedloop.html#a0f6384c9ba1b53a4c97b27fe69870778", null ],
    [ "get_Kp", "classclosedloop_1_1closedloop.html#a92a28bd706ea105dbeb3229fb24398b3", null ],
    [ "set_Kp", "classclosedloop_1_1closedloop.html#af32864b927a79493995c8941c1d3ba8d", null ],
    [ "update", "classclosedloop_1_1closedloop.html#aae9dd58008943f76db8a9d888e8c5582", null ],
    [ "Kp", "classclosedloop_1_1closedloop.html#a0a5380e2acf010d4993a35e20b38a12e", null ],
    [ "max_val", "classclosedloop_1_1closedloop.html#a677b7471b83126e64aa97bbf40d4d47a", null ],
    [ "min_val", "classclosedloop_1_1closedloop.html#a998110ae30018d6b5c15b4283fa41e67", null ]
];