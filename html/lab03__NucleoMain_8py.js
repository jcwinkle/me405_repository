var lab03__NucleoMain_8py =
[
    [ "button_press", "lab03__NucleoMain_8py.html#a2af087bc06ad91b402bee4cc9790faa5", null ],
    [ "adc", "lab03__NucleoMain_8py.html#afba936cb6fd30c64a3b6ff7635ec2ce1", null ],
    [ "buffer", "lab03__NucleoMain_8py.html#a746b03b5ba68567cc3967496e0669406", null ],
    [ "Button", "lab03__NucleoMain_8py.html#a30a4d21c264cd13f19f5bbabe341a636", null ],
    [ "button_data", "lab03__NucleoMain_8py.html#a51f7b6e9179eb4249c6a0b4197fc0bc0", null ],
    [ "extint", "lab03__NucleoMain_8py.html#a61c2f2f77f91d6c1558d0409663d709c", null ],
    [ "flag", "lab03__NucleoMain_8py.html#a220bf6e08ff10999404d90532e6926cf", null ],
    [ "sample_freq", "lab03__NucleoMain_8py.html#a4c59d0b1bef55508daac4fa5793f49e4", null ],
    [ "sample_range", "lab03__NucleoMain_8py.html#a0e5922e626e9d7eccc3d4b5668498a02", null ],
    [ "tim6", "lab03__NucleoMain_8py.html#a2e6e4033360dc8d2ed6f07de905c35d7", null ],
    [ "time_data", "lab03__NucleoMain_8py.html#a5071d3975ba5af9b9383284dc32f4885", null ],
    [ "uart", "lab03__NucleoMain_8py.html#a577da45f6a8ff1086684ae56324cc10d", null ],
    [ "val", "lab03__NucleoMain_8py.html#aef5e62e67901f2333ac7109d9f618160", null ]
];