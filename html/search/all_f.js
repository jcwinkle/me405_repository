var searchData=
[
  ['sample_5ffreq_67',['sample_freq',['../lab03__NucleoMain_8py.html#a4c59d0b1bef55508daac4fa5793f49e4',1,'lab03_NucleoMain.sample_freq()'],['../main_8py.html#a6977e38c0733de27560264d8ad0d520c',1,'main.sample_freq()']]],
  ['sample_5frange_68',['sample_range',['../lab03__NucleoMain_8py.html#a0e5922e626e9d7eccc3d4b5668498a02',1,'lab03_NucleoMain.sample_range()'],['../main_8py.html#a663a80c1af515887e5947237a2abb991',1,'main.sample_range()']]],
  ['set_5fduty_69',['set_duty',['../classMotorDriver_1_1MotorDriver.html#a4bb86eafa05d8e874896aef624ad14cd',1,'MotorDriver::MotorDriver']]],
  ['set_5fposition_70',['set_position',['../classEncoderDriver_1_1EncoderDriver.html#a6f57b63e0509f7fc3c162714717476fc',1,'EncoderDriver::EncoderDriver']]],
  ['start_5ftime_71',['start_time',['../lab04__NucleoMain_8py.html#a9db72fa77b166b81f9fa4b2f6afd2756',1,'lab04_NucleoMain']]],
  ['state_72',['state',['../lab01__Vendotron_8py.html#a53531aad8f72872956ec69ec91faa679',1,'lab01_Vendotron']]],
  ['storedata_73',['storeData',['../projectFile_8py.html#a87a6ad03bc3ba0c05a5978cab9c6563d',1,'projectFile']]]
];
