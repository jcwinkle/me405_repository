var searchData=
[
  ['temp_5famb_173',['temp_amb',['../lab04__NucleoMain_8py.html#aa7f960e30b2fa15e3e0cb23f3d639f13',1,'lab04_NucleoMain']]],
  ['temp_5fdata_174',['temp_data',['../lab04__NucleoMain_8py.html#a3bb8b489f8a133fcd8044f50d9a9fa79',1,'lab04_NucleoMain']]],
  ['temp_5fint_175',['temp_int',['../lab04__NucleoMain_8py.html#abb72aa05e397ac23fdae5f2b97fcb78a',1,'lab04_NucleoMain']]],
  ['tim2_176',['tim2',['../lab02__ReactionTimer_8py.html#a5662daf8881d62975e08bab121b408a8',1,'lab02_ReactionTimer']]],
  ['tim6_177',['tim6',['../main_8py.html#a34fa6cad72d11da527dcab62101a2eb8',1,'main']]],
  ['time_178',['time',['../lab04__NucleoMain_8py.html#ae354058864da6f929181c6c8e19afbc9',1,'lab04_NucleoMain.time()'],['../lab04__TempDataPlot_8py.html#a1a3438339fd696594f6a5655c1cbc1ae',1,'lab04_TempDataPlot.time()']]],
  ['time_5fdata_179',['time_data',['../lab03__NucleoMain_8py.html#a5071d3975ba5af9b9383284dc32f4885',1,'lab03_NucleoMain.time_data()'],['../main_8py.html#a8d6a0486d90b42beb143dc94476b15fb',1,'main.time_data()']]],
  ['time_5fdiff_180',['time_diff',['../lab04__NucleoMain_8py.html#a0632491dd275b67a5ee20e0ecb1d2976',1,'lab04_NucleoMain']]],
  ['time_5fs_181',['time_s',['../lab04__NucleoMain_8py.html#a7c8cab17582397d380d04b1c62541d4d',1,'lab04_NucleoMain']]],
  ['total_5ftime_182',['total_time',['../lab04__NucleoMain_8py.html#ac6cac225934eac513a9bc0b2c85be9eb',1,'lab04_NucleoMain']]]
];
