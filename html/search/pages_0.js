var searchData=
[
  ['lab_201_3a_20vendotron_20finite_20state_20machine_187',['Lab 1: Vendotron Finite State Machine',['../page_lab1.html',1,'']]],
  ['lab_202_3a_20nucleo_20reaction_20timer_188',['Lab 2: Nucleo Reaction Timer',['../page_lab2.html',1,'']]],
  ['lab_203_3a_20nucleo_20push_20button_20voltage_20response_189',['Lab 3: Nucleo Push Button Voltage Response',['../page_lab3.html',1,'']]],
  ['lab_204_3a_20temperature_20data_20collection_20on_20nucleo_190',['Lab 4: Temperature Data Collection on Nucleo',['../page_lab4.html',1,'']]],
  ['lab_205_3a_20feeling_20tipsy_3f_20_28motor_20controlled_20balancing_20platform_20hand_20calcs_29_191',['Lab 5: Feeling Tipsy? (Motor Controlled Balancing Platform Hand Calcs)',['../page_lab5.html',1,'']]],
  ['lab_206_3a_20simulation_20or_20reality_3f_20_28pt_2e_201_3a_20linearization_29_192',['Lab 6: Simulation or Reality? (Pt. 1: Linearization)',['../page_lab6.html',1,'']]],
  ['lab_206_3a_20simulation_20or_20reality_3f_20_28pt_2e_202_3a_20matlab_2fsimulink_20simulatons_29_193',['Lab 6: Simulation or Reality? (Pt. 2: MATLAB/Simulink Simulatons)',['../page_lab6b.html',1,'']]],
  ['lab_207_3a_20feeling_20touchy_3f_194',['Lab 7: Feeling Touchy?',['../page_lab7.html',1,'']]],
  ['lab_208_3a_20motordriver_20and_20encoder_20classes_195',['Lab 8: Motordriver and Encoder classes',['../page_lab8.html',1,'']]]
];
