var searchData=
[
  ['lab01_5fvendotron_2epy_39',['lab01_Vendotron.py',['../lab01__Vendotron_8py.html',1,'']]],
  ['lab02_5freactiontimer_2epy_40',['lab02_ReactionTimer.py',['../lab02__ReactionTimer_8py.html',1,'']]],
  ['lab03_5fbuttonresponseplot_2epy_41',['lab03_ButtonResponsePlot.py',['../lab03__ButtonResponsePlot_8py.html',1,'']]],
  ['lab03_5fnucleomain_2epy_42',['lab03_NucleoMain.py',['../lab03__NucleoMain_8py.html',1,'']]],
  ['lab04_5fmcu_5ftemp_5ftest_2epy_43',['lab04_MCU_temp_test.py',['../lab04__MCU__temp__test_8py.html',1,'']]],
  ['lab04_5fnucleomain_2epy_44',['lab04_NucleoMain.py',['../lab04__NucleoMain_8py.html',1,'']]],
  ['lab04_5ftempdataplot_2epy_45',['lab04_TempDataPlot.py',['../lab04__TempDataPlot_8py.html',1,'']]],
  ['led_46',['LED',['../lab02__ReactionTimer_8py.html#a2ca936774c270e6f57b5d4904f9e798d',1,'lab02_ReactionTimer']]],
  ['line_47',['line',['../lab04__TempDataPlot_8py.html#ad40d6237ff87e961c5f800722b969b67',1,'lab04_TempDataPlot']]],
  ['lab_201_3a_20vendotron_20finite_20state_20machine_48',['Lab 1: Vendotron Finite State Machine',['../page_lab1.html',1,'']]],
  ['lab_202_3a_20nucleo_20reaction_20timer_49',['Lab 2: Nucleo Reaction Timer',['../page_lab2.html',1,'']]],
  ['lab_203_3a_20nucleo_20push_20button_20voltage_20response_50',['Lab 3: Nucleo Push Button Voltage Response',['../page_lab3.html',1,'']]],
  ['lab_204_3a_20temperature_20data_20collection_20on_20nucleo_51',['Lab 4: Temperature Data Collection on Nucleo',['../page_lab4.html',1,'']]],
  ['lab_205_3a_20feeling_20tipsy_3f_20_28motor_20controlled_20balancing_20platform_20hand_20calcs_29_52',['Lab 5: Feeling Tipsy? (Motor Controlled Balancing Platform Hand Calcs)',['../page_lab5.html',1,'']]],
  ['lab_206_3a_20simulation_20or_20reality_3f_20_28pt_2e_201_3a_20linearization_29_53',['Lab 6: Simulation or Reality? (Pt. 1: Linearization)',['../page_lab6.html',1,'']]],
  ['lab_206_3a_20simulation_20or_20reality_3f_20_28pt_2e_202_3a_20matlab_2fsimulink_20simulatons_29_54',['Lab 6: Simulation or Reality? (Pt. 2: MATLAB/Simulink Simulatons)',['../page_lab6b.html',1,'']]],
  ['lab_207_3a_20feeling_20touchy_3f_55',['Lab 7: Feeling Touchy?',['../page_lab7.html',1,'']]],
  ['lab_208_3a_20motordriver_20and_20encoder_20classes_56',['Lab 8: Motordriver and Encoder classes',['../page_lab8.html',1,'']]]
];
