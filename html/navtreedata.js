/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Jacob Winkler's ME 405 Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "References", "index.html#sec_ref", null ],
    [ "Lab 1: Vendotron Finite State Machine", "page_lab1.html", [
      [ "Description", "page_lab1.html#page_lab1_desc", null ],
      [ "Source Code", "page_lab1.html#page_lab1_sourcecode", null ],
      [ "Documentation", "page_lab1.html#page_lab1_doc", null ]
    ] ],
    [ "Lab 2: Nucleo Reaction Timer", "page_lab2.html", [
      [ "Description", "page_lab2.html#page_lab2_desc", null ],
      [ "Source Code", "page_lab2.html#page_lab2_sourcecode", null ],
      [ "Documentation", "page_lab2.html#page_lab2_doc", null ]
    ] ],
    [ "Lab 3: Nucleo Push Button Voltage Response", "page_lab3.html", [
      [ "Description", "page_lab3.html#page_lab3_desc", null ],
      [ "Sample Plot", "page_lab3.html#page_lab3_plot", null ],
      [ "Sample CSV File", "page_lab3.html#page_lab3_csv", null ],
      [ "Source Code", "page_lab3.html#page_lab3_sourcecode", null ],
      [ "Documentation", "page_lab3.html#page_lab3_doc", null ]
    ] ],
    [ "Lab 4: Temperature Data Collection on Nucleo", "page_lab4.html", [
      [ "Description", "page_lab4.html#page_lab4_desc", null ],
      [ "Sample Plot", "page_lab4.html#page_lab4_plot", null ],
      [ "Sample CSV File", "page_lab4.html#page_lab4_csv", null ],
      [ "Source Code", "page_lab4.html#page_lab4_sourcecode", null ],
      [ "Documentation", "page_lab4.html#page_lab4_doc", null ]
    ] ],
    [ "Lab 5: Feeling Tipsy? (Motor Controlled Balancing Platform Hand Calcs)", "page_lab5.html", [
      [ "Description", "page_lab5.html#page_lab5_desc", null ],
      [ "Analysis", "page_lab5.html#page_lab5_analysis", null ]
    ] ],
    [ "Lab 6: Simulation or Reality? (Pt. 1: Linearization)", "page_lab6.html", [
      [ "Description", "page_lab6.html#page_lab6_desc", null ],
      [ "Partnership", "page_lab6.html#page_lab6_partner", null ],
      [ "Linearization - Hand Calcs", "page_lab6.html#page_lab6_linearization", null ]
    ] ],
    [ "Lab 6: Simulation or Reality? (Pt. 2: MATLAB/Simulink Simulatons)", "page_lab6b.html", [
      [ "Source Code", "page_lab6b.html#page_lab6_sourcecode", null ],
      [ "Open Loop Case 1", "page_lab6b.html#page_lab6_OLcase1", null ],
      [ "Open Loop Case 2", "page_lab6b.html#page_lab6_OLcase2", null ],
      [ "Open Loop Case 3", "page_lab6b.html#page_lab6_OLcase3", null ],
      [ "Open Loop Case 4", "page_lab6b.html#page_lab6_OLcase4", null ],
      [ "Closed Loop Case", "page_lab6b.html#page_lab6_CLcase", null ]
    ] ],
    [ "Lab 7: Feeling Touchy?", "page_lab7.html", [
      [ "Description", "page_lab7.html#page_lab7_desc", null ],
      [ "Source Code", "page_lab7.html#page_lab7_sourcecode", null ],
      [ "Documentation", "page_lab7.html#page_lab7_doc", null ]
    ] ],
    [ "Lab 8: Motordriver and Encoder classes", "page_lab8.html", [
      [ "Description", "page_lab8.html#page_lab8_desc", null ],
      [ "Partnership", "page_lab8.html#page_lab8_partner", null ],
      [ "Source Code", "page_lab8.html#page_lab8_sourcecode", null ],
      [ "Documentation", "page_lab8.html#page_lab8_doc", null ]
    ] ],
    [ "Term Project - Balancing Table", "page_TermProject.html", [
      [ "Description", "page_TermProject.html#page_TermProject_desc", null ],
      [ "Results", "page_TermProject.html#page_TermProject_results", null ],
      [ "User Manual", "page_TermProject.html#page_TermProject_usermanual", null ],
      [ "Partnership", "page_TermProject.html#page_TermProject_partnership", null ],
      [ "References", "page_TermProject.html#page_TermProject_references", null ],
      [ "Source Code", "page_TermProject.html#page_TermProject_sourcecode", null ],
      [ "Documentation", "page_TermProject.html#page_TermProject_documentation", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"EncoderDriver_8py.html",
"projectFile_8py.html#a1b9f054ddb61f05d8c0b600d98150d55"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';