var lab04__TempDataPlot_8py =
[
    [ "Amb", "lab04__TempDataPlot_8py.html#a8ba2596bef779d94076b3edbbfb7e584", null ],
    [ "AmbTemp", "lab04__TempDataPlot_8py.html#a88e9d07b07e613079e39a9af4727fb08", null ],
    [ "avgAmb", "lab04__TempDataPlot_8py.html#a7b1919d739f6c16c73fcbadce7d6479f", null ],
    [ "avgInt", "lab04__TempDataPlot_8py.html#a04ee5e4cff36a9d314b533b0ee114f48", null ],
    [ "data", "lab04__TempDataPlot_8py.html#a40e8b1c7ad5cc7b1aa2f929e2bc4338f", null ],
    [ "hspace", "lab04__TempDataPlot_8py.html#a9de3168681a7af4fe47b5d2d296c2d4a", null ],
    [ "Int", "lab04__TempDataPlot_8py.html#a3e37d349741d97a599fcfd5dd81cb839", null ],
    [ "IntTemp", "lab04__TempDataPlot_8py.html#a353a6d32fb026ddcc14a333acbcb2014", null ],
    [ "line", "lab04__TempDataPlot_8py.html#ad40d6237ff87e961c5f800722b969b67", null ],
    [ "t", "lab04__TempDataPlot_8py.html#af3cd1071c8f861db82bad2860eb48b1b", null ],
    [ "time", "lab04__TempDataPlot_8py.html#a1a3438339fd696594f6a5655c1cbc1ae", null ]
];