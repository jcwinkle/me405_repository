'''
@file lab04_NucleoMain.py
@brief File on nulceo which collects MCU and ambiant temperature data and records it in a CSV file
@details This code runs on the Nucleo L476RG and uses an ADC object to measure 
         two different temperature values every 60 seconds until the user interupts
         the program internal temperature of the Nucleo and uses I2C 
         Communication with an mcp9808 temperature sensor to record the 
         surrounding ambiant room temperature. Both temperatures are recorded 
         in degrees celcius along with their corresponding timestamp in seconds. 
         (Note: the recorded timestamp is the time after starting the data 
          collection)
@author Jacob Winkler
@date February 3, 2021
'''

import utime
import pyb
from mcp9808 import mcp9808

## Creates ADC object with 12 bit resolution, internal channels
adcall = pyb.ADCAll(12, 0x70000)
 
# Run a vref check to fix error with read_core_temp
adcall.read_vref()

## Initialize Data File as a CSV
filename = 'Raw_Temp_Data_Nucleo.csv'

## Time when Nucleo temperature data collection began as an integer number of milliseconds
start_time = utime.ticks_ms()

## Iterating ticks value (starts equal to start_time) as an integer number of milliseconds
prev_time = start_time

## The Iteration time (time of the current Nucleo temperature data sample) since the start as an integer number of milliseconds
time = 0

## Initialize mcp9808 Driver
mcp9808 = mcp9808(24)


with open (filename,"w") as csv_file:
    print('The Nucleo is currently taking temperature data! Press CTRL-C to stop data collection!')
    header = 'Time (sec), Nucleo Core Temperature (degC), Ambient Room Temperature (degC)\n'
    print('The format is: "'+ header+'"')
    csv_file.write(header)
    while True:
        try:
            ## Internal temperature as recorded by the nucleo as an integer number in degrees celcius
            temp_int = adcall.read_core_temp()
            ## Ambiant temperature as recorded by the mcp9808 as an integer number in degrees celcius
            temp_amb = mcp9808.celcius()
            ## Timestamp of current data sample as an integer number of milliseconds
            curr_time = utime.ticks_ms()
            ## Amount of time that passed since the last data sample as an integer number of milliseconds
            time_diff = utime.ticks_diff(curr_time, prev_time)
            prev_time = curr_time
            time += time_diff
            ## Iteration time as an integer number of seconds
            time_s = time/1000
            
            ## Temperature Data string contsining the sample time, Core Temp, 
            # and Ambiant Temp(changes every iteration)
            temp_data = str(time_s) + ', ' + str(temp_int) + ', ' + str(temp_amb) + '\n'
            # Write data to csv file
            csv_file.write(temp_data)
            
            print(temp_data)
            
            # Sleep for 60 seconds in between data samples
            utime.sleep(60)
        except KeyboardInterrupt:
            ## Total data collection time as an integer number of milliseconds
            total_time = utime.ticks_diff(utime.ticks_ms(), start_time)
            total_secs = total_time/1000
            total_hrs = total_time/3.6E6
            print('Data Collection Time: ' + str(total_secs) + ' seconds  (' + str(total_hrs) + ' hours).' )
            break       
print('The ' + filename + ' file containing the most recent temperature data has been stored on the Nucleo')