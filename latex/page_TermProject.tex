\hypertarget{page_TermProject_page_TermProject_desc}{}\doxysection{Description}\label{page_TermProject_page_TermProject_desc}
The goal of this project is to balance a ball on a plate which is free to rotate in two dimensions. To do this, the position of the ball on the plate is read using a resistive touch panel and the Touch\+Panel class developed in \mbox{\hyperlink{page_lab7}{Lab 7\+: Feeling Touchy?}}, angular position and velocity of the table is read from encoders using the Encoder\+Driver class developed in \mbox{\hyperlink{page_lab8}{Lab 8\+: Motordriver and Encoder classes}}, and motors (one controlling the X-\/tilt of the plate and one controlling the Y-\/tilt) are actuated using the Motor\+Driver class, also developed in lab 8. However, in order to actuate the motors with the appropriate duty cycle to balance a ball, proper feedback gains for each system variable (table angle, table angular velocity, ball position, and ball velocity) must be used.

To theoretically calculate the gains to control the balancing plate system, a M\+A\+T\+L\+AB script was created. Inputting dimensions from the system and the initial hand calculations from \mbox{\hyperlink{page_lab5}{Lab 5\+: Feeling Tipsy? (Motor Controlled Balancing Platform Hand Calcs)}}, the state space matrices are defined, and the feedback loop gains are symbolically calculated for each state space variable based on u = -\/K$\ast$x. From there, the closed loop gains are compared to an ideal system with a desired damping ratio and natural frequency. The system gains are then computed using polynomial matching between the transfer function coefficients of the actual system (solved symbolically) and the coefficients of ideal system transfer function. See the source code section below for a link to the M\+A\+T\+L\+AB script.

Implementing these gains into a micropython script, 6 main methods were created to control the board. The Xtable and Ytable methods use the system variables (read using the touch panel and encoder classes described below) and gains (hand-\/tuned based on the ones generated in the M\+A\+T\+L\+AB script) to calculate the appropriate duty cycle to apply to each motor to balance the ball in the center of the plate. The touch panel method reads the X position, X velocity, Y position, and Y velocity of the ball on the plate. The encoder method reads the angle and rotational speed of the X and Y motor shafts and converts it plate angle and angular velocity in both the X and Y directions. Alternatively, another method that calls an I\+MU to measure plate orientation could be used instead of the encoder task. To switch between these methods, the initialization variable named EI in the controller code must be switched (EI = 0 runs the encoder method and EI = 1 runs the I\+MU method). Lastly, the C\+SV writer method writes the all of data read by the methods described above to a C\+SV file for later plotting and analysis.

To operate these task methods cooperatively, a cooperative-\/multitasking script developed by Professor Ridgely was utilized. The multitasking script operates each of the above tasks at pre-\/set periods, running the tasks according to a set priority. These tasks are defined in the initialization portion of our script, then operated with the Professor Ridgely\textquotesingle{}s scheduler in a \textquotesingle{}while True\+:\textquotesingle{} loop. The motor fault funcitonality is also operated in this loop.\hypertarget{page_TermProject_page_TermProject_results}{}\doxysection{Results}\label{page_TermProject_page_TermProject_results}
With a great deal of iterative tuning of the system, the table was able to balance itself in both directions with relative ease and balance the ball for small deflections. Tuning of the saturation limits of the duty cycle of the motor helped to limit the occurrence of motor faults, however they still occur for large table deflections. Large ball deflections tend to introduce instability into the system that inevitably results in a motor fault. A plot of the system behavior is shown below\+:



As can be seen in the plot, the table returns to a stable orientation after some oscillation for relatively small table displacements. Once the ball is introduced, the table manages to stop the motion of the ball but not return it perfectly to the center.

The resolution of the data is limited by the mechanics of the data collection task. As the majority of time spent on this project was dedicated to improving system stability, the data collection system has not been refined and only collects data every quarter-\/second. As such, it is sufficient to display general trends and little more.

It is also noteworthy that the ability of the system to respond to large motions is limited due to the faulting of the motor. A saturation limit is implemented in the script to limit the duty cycle to a level that will not cause near-\/continuous motor faulting, iteratively refined to an optimal limit of 60\%. This duty cycle provides sufficient torque to run the system, if only barely. Additionally, the torque provided below approximately 40\% duty cycle is not sufficient to affect the system at all. As such, the motors operate in a relatively narrow band of torques, resulting in a system non-\/linearity that is not accounted for in our theoretical model.

Due to the nonlinearities in the model and other inconsistancies in the comparison between reality and theoritical predictions, each gain value had to be tuned to provide a reasonable response. However, the tuning process never allowed the system to operate at angles larger than approximately 10 degrees without becoming unstable.\hypertarget{page_TermProject_page_TermProject_usermanual}{}\doxysection{User Manual}\label{page_TermProject_page_TermProject_usermanual}
To run the balancing table, first ensure all the necessary files are on the nucleo. The main file may be run with the \textquotesingle{}execfile()\textquotesingle{} funciton or renamed \textquotesingle{}\mbox{\hyperlink{main_8py}{main.\+py}}.\textquotesingle{} In either case, the script will send a prompt to the serial terminal prior to activating the motors. If encoder angle tracking is enabled, the user will be prompted to level the table in order to zero the encoders at the correct position.

Once the program is in operation, it will attempt to return the table to the level position. For minimal deflections and the ball remaining close to the center the script will run as intended. For larger deviations, however, the motors may enter a situation where the overcurrent fault circut trips. In this circumstance, an external interrupt is triggered on the board that stops the motors. The user will be prompted to type \textquotesingle{}Fault Cleared\textquotesingle{} into the serial terminal in order to reset the table. This is to ensure that the user manually shifts the table to clear it from the faulted position prior to resuming operation.

Additional parameters that may need to be adjusted cannot be modified through the terminal and may be manually editied in the script. These should only be modified by users who are familiar with the project scenario and this script in particular. The first four parameters are boolean flags that exist in the \textquotesingle{}if {\bfseries{name}} == \textquotesingle{}{\bfseries{main}}\textquotesingle{} section of the script. the first two, \textquotesingle{}Rx\textquotesingle{} and \textquotesingle{}Ry\textquotesingle{}, which enable the controllers for each axis of the table, labeled according to the corresponding touch panel axis. The third toggles between encoder-\/based angle measurement and inertial measurement unit (I\+MU) angle measurement, labeled \textquotesingle{}EI\textquotesingle{}. The I\+MU is enabled when this boolean is 1 and the encoders are enabled when it is 0. The final boolean, \textquotesingle{}CD\textquotesingle{} enables the collection of data from the sensors, stored in a corresponding C\+SV file. The remaining adjustable parameters are controller gains, housed in the controller methods near the beginning of the script. These gains may be tuned as necessary for individual hardware sets, as optimal gains tend to be hardware specific. Unfortunately, not all hardware is created equal...

If data collection is enabled, wait until the terminal indicates that collection has begun before placing the ball on the touch panel. This will ensure that the system response is captured. The data is stored in a C\+SV file, which may be collected from the board by a program such as ampy. ~\newline
 To end the program, press ctl+c. This will disable the motors and the motor interrupt, then indicate that the program is complete. ~\newline
 \hypertarget{page_TermProject_page_TermProject_partnership}{}\doxysection{Partnership}\label{page_TermProject_page_TermProject_partnership}
This lab was completed in collaboration with fellow engineer, Rick Hall. His corresponding documentation page can be found at\+: ~\newline
 \href{https://rhall09.bitbucket.io/page_TermProject.html}{\texttt{ https\+://rhall09.\+bitbucket.\+io/page\+\_\+\+Term\+Project.\+html}}

However, it should be noted that for this lab, we both just pushed to Jacob\textquotesingle{}s repository (source code linked below). ~\newline
 \hypertarget{page_TermProject_page_TermProject_references}{}\doxysection{References}\label{page_TermProject_page_TermProject_references}
This script utilizes code from a number of sources\+: ~\newline
 ~\newline

\begin{DoxyEnumerate}
\item Cooperative Multitasking Software, by Professor JR Ridgely ~\newline
~\newline
 This set of classes was utilized to provide timing for the tasks in our script. It was developed by Professor JR Ridgely as a scheduling file example for M\+E405, and has been minimally modified for use in our script. His code can be found at the link below\+: ~\newline
 \href{https://bitbucket.org/jcwinkle/me405_repository/src/master/Final\%20Project\%20Files/scheduler_files/}{\texttt{ https\+://bitbucket.\+org/jcwinkle/me405\+\_\+repository/src/master/\+Final\%20\+Project\%20\+Files/scheduler\+\_\+files/}} ~\newline
~\newline
~\newline

\item B\+N\+O055 Inertial Measurement Unit Drivers, from Adafruit Industries ~\newline
~\newline
 This hardware driver was utilized in our script to expedite the use of the Adafruit B\+N\+O055 I\+MU. It was developed by Radomir Dopieralski for Adafruit Industries and extended for Micro\+Python by Peter Hinch. This code is stored in our repository at the link below\+: ~\newline
 \href{https://bitbucket.org/jcwinkle/me405_repository/src/master/Final\%20Project\%20Files/BNO055\%20IMU/}{\texttt{ https\+://bitbucket.\+org/jcwinkle/me405\+\_\+repository/src/master/\+Final\%20\+Project\%20\+Files/\+B\+N\+O055\%20\+I\+M\+U/}} ~\newline
 The original source may be found at the github link below\+: ~\newline
 \href{https://github.com/micropython-IMU/micropython-bno055}{\texttt{ https\+://github.\+com/micropython-\/\+I\+M\+U/micropython-\/bno055}} ~\newline
~\newline
~\newline

\item Touch Panel, Motor, and Encoder drivers from previous labs\+: ~\newline
~\newline
 Drivers developed in earlier M\+E405 labs by Rick Hall and/or Jacob Winkler were used to run the corresponding hardware. The drivers can be found at the following links\+: ~\newline
~\newline
 \mbox{\hyperlink{page_lab7}{Lab 7\+: Feeling Touchy?}} ~\newline
 Touch\+Panel -\/ \href{https://bitbucket.org/jcwinkle/me405_repository/src/master/Final\%20Project\%20Files/TouchPanel.py}{\texttt{ https\+://bitbucket.\+org/jcwinkle/me405\+\_\+repository/src/master/\+Final\%20\+Project\%20\+Files/\+Touch\+Panel.\+py}} ~\newline
~\newline
 \mbox{\hyperlink{page_lab8}{Lab 8\+: Motordriver and Encoder classes}} ~\newline
 Motor\+Driver -\/ \href{https://bitbucket.org/jcwinkle/me405_repository/src/master/Final\%20Project\%20Files/MotorDriver.py}{\texttt{ https\+://bitbucket.\+org/jcwinkle/me405\+\_\+repository/src/master/\+Final\%20\+Project\%20\+Files/\+Motor\+Driver.\+py}} ~\newline
 Encoder\+Driver -\/ \href{https://bitbucket.org/jcwinkle/me405_repository/src/master/Final\%20Project\%20Files/EncoderDriver.py}{\texttt{ https\+://bitbucket.\+org/jcwinkle/me405\+\_\+repository/src/master/\+Final\%20\+Project\%20\+Files/\+Encoder\+Driver.\+py}}
\end{DoxyEnumerate}\hypertarget{page_TermProject_page_TermProject_sourcecode}{}\doxysection{Source Code}\label{page_TermProject_page_TermProject_sourcecode}
The source code for the main script can be found in the link below\+: ~\newline
 \href{https://bitbucket.org/jcwinkle/me405_repository/src/master/Final\%20Project\%20Files/projectFile.py}{\texttt{ https\+://bitbucket.\+org/jcwinkle/me405\+\_\+repository/src/master/\+Final\%20\+Project\%20\+Files/project\+File.\+py}}\hypertarget{page_TermProject_page_TermProject_documentation}{}\doxysection{Documentation}\label{page_TermProject_page_TermProject_documentation}
The main script documentation is included at the link below\+: ~\newline

\begin{DoxyItemize}
\item Term Project Main Script\+: \mbox{\hyperlink{projectFile_8py}{project\+File.\+py}} 
\end{DoxyItemize}