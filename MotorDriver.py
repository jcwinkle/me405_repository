''' 
@file MotorDriver.py
@brief File defining generic Motor Driver class
@author Jacob Winkler (adapted from code by Charlie Refvem)
@date March 4, 2021
'''

import pyb
import utime

class MotorDriver:
    ''' 
    This class implements a motor driver for the ME405 PCB board. 
    '''
    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, motor_num=1):
        ''' 
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin. Input must be a string!!!
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1. Input must be a string!!!
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2. Input must be a string!!!
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        @param motor_num An interger input specifying which motor the driver is actuating. 
                         defaults to motor 1 unless specified. Note: each timer can only actuate 2 motors, so the only valid inputs are 1 or 2.
        
        '''
        self.motor_num = motor_num
        self.pin_nSLEEP = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        pin1 = pyb.Pin(IN1_pin)
        pin2 = pyb.Pin(IN2_pin)
        tim = pyb.Timer(timer, freq=20000)
        self.duty = 0
        
        if self.motor_num == 1:
            self.chan1 = tim.channel(1, pyb.Timer.PWM, pin=pin1)
            self.chan2 = tim.channel(2, pyb.Timer.PWM, pin=pin2)
        elif self.motor_num == 2:
            self.chan1 = tim.channel(3, pyb.Timer.PWM, pin=pin1)
            self.chan2 = tim.channel(4, pyb.Timer.PWM, pin=pin2)
        else:
            print('Motor number invalid. Each time can only operate 2 motors!')
            
        self.chan1.pulse_width_percent(0)
        self.chan2.pulse_width_percent(0)
        self.pin_nSLEEP.low()
        print ('Creating the Motor driver')

    def enable (self,In_IRQ=False):
        ''' 
        @brief Sets pin_nSLEEP to high, allowing motor to be actuated
        '''
        pyb.disable_irq()
        
        self.pin_nSLEEP.high()
        
        if not In_IRQ:
            print ('Motor ' + str(self.motor_num) + ' enabled')
        
        pyb.enable_irq()

    def disable (self,In_IRQ=False):
        ''' 
        @brief Sets pin_nSLEEP to high, disabling the motor from being actuated
        '''
        pyb.disable_irq()
        
        self.pin_nSLEEP.low()
        self.set_duty(0)
        
        if not In_IRQ:
            print ('Motor ' + str(self.motor_num) + ' disabled')

        pyb.enable_irq()

    def set_duty (self, duty):
        ''' 
        @brief This method sets the duty cycle to be sent to the motor to the given level. 
        @details Positive values cause effort in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor
        
        '''
        
        self.duty = duty
            
        if duty >= 0:
            self.chan1.pulse_width_percent(duty)
            self.chan2.pulse_width_percent(0)
        elif duty < 0:
            duty = abs(duty)
            self.chan1.pulse_width_percent(0)
            self.chan2.pulse_width_percent(duty)
        

if __name__ == '__main__':
    
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = 'A15'
    pin_IN1 = 'B4'
    pin_IN2 = 'B5'
    pin_IN3 = 'B0'
    pin_IN4 = 'B1'

    # Create the timer objects used for PWM generation
    tim = 3
    
    # Create motor objects passing in the pins and timer
    mot_1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim, 1)
    mot_2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, tim, 2)
    
    # initially, neither of the motors have faults
    fault = 0
    In = ''
    
    
    def fault_check(line):
        '''
        @brief This function calls when the nFault pin, pin B2, is low, indicating an overcurrrent condition in the motor.
        @param Line This param allows for functionality of the interrupt service request.
        '''
        # this code disables both motors and turns the fault variable to 1, 
        # triggering the motor fault user interface
        global fault
        mot_1.disable()
        mot_2.disable()
        fault = 1
        
    # Define external interupt to stop motors if there is a fault. The 
    # nFAULT pin, pin B2 on the motordriver chip, automatically detects motor 
    # faults (overcurrent conditions). It is active low, meaning that a low 
    # signal indicates a fault. 
    extint = pyb.ExtInt(pyb.Pin(pyb.Pin.board.PB2, mode=pyb.Pin.IN),
                        pyb.ExtInt.IRQ_FALLING, 
                        pyb.Pin.PULL_NONE, 
                        callback = fault_check)
    

    
    while True:
        try:
            cycleX = int(input('Input desired duty cycle for X:   '))
            cycleY = int(input('Input desired duty cycle for Y:   '))
            # Enable the motors 
            mot_1.enable()
            mot_2.enable()
            while True:
                try:
                    if fault == 0:
                        #print('Spinning Motors...')
                        # spin motor 1 clockwise at 70% speed
                        mot_1.set_duty(cycleX)
                        # spin motor 2 counterclockwise at 70% speed
                        mot_2.set_duty(cycleY)
                        
                        In = ''
                    
                    elif fault == 1:
                        extint.disable()
                        # User interface to deal with motor faults
                        if In != 'Fault Cleared':
                            print('A fault has occured in your motor!!! Check your hardware and fix the fault before continuing.')
                            In = input('Type "Fault Cleared" to resume motors.\n>>> ')
                            
                        else: 
                            print('Resuming motors...')
                            utime.sleep(1)
                            mot_1.enable()
                            mot_2.enable()
                            
                            fault = 0
                            extint.enable()
                    else:
                        # this state should never trigger, as fault should always be 
                        # either 1 or 0
                        pass
                    
                except KeyboardInterrupt:
                    print('Stopping and Disabling motors...')
                    mot_1.set_duty(0)
                    mot_2.set_duty(0)
                    mot_1.disable()
                    mot_2.disable()
                    break
        except KeyboardInterrupt:
            break
                
    
    
    