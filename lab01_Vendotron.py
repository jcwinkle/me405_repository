'''
@file lab01_Vendotron.py
@brief File defining the Vending Machine, Vendotron^TM^
@details Contains the functions getChange and printWelcome, along with code 
         implemented as a Finite State Machine to control Vendotron^TM^. 
@author Jacob Winkler
@date January 17, 2021
'''

# Initialize code to run FSM (not the init state). 
# This code initializes the program, not the FSM

## The variable representing the current state of the Finite State Machine
state = 0
import keyboard 

last_key = ''

def kb_cb(key):
    '''
    @brief Callback function which is called when a Key has been pressed
    @param key The key on the computer keyboard that has been pressed
    '''
    
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("e", callback=kb_cb)
keyboard.on_release_key("c", callback=kb_cb)
keyboard.on_release_key("p", callback=kb_cb)
keyboard.on_release_key("s", callback=kb_cb)
keyboard.on_release_key("d", callback=kb_cb)
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)


def getChange(price, payment):
    '''
    @brief Computes change for monetary transaction
    @details Computes change given set of bills/coins and returns minimum 
             amount of denominations as change
    @param price An integer number of cents representing the price of an item
    @param payment An 8-element list representing the integer number of 
                   pennies, nickels, dimes, quarters, ones, fives, tens, and 
                   twenties, in that order, paid to purchase an item
    @return if payment value is sufficient, returns a list of bills/coins and 
            an integer of the total value of the change in cents. if payment is
            insufficient, returns None. 
    '''
    val_pennies = 1*payment[0]
    val_nickels = 5*payment[1]
    val_dimes = 10*payment[2]
    val_quarters = 25*payment[3]
    val_ones = 100*payment[4]
    val_fives = 500*payment[5]
    val_tens = 1000*payment[6]
    val_twenties = 2000*payment[7]
    payment_value = val_pennies + val_nickels + val_dimes + val_quarters + val_ones + val_fives + val_tens + val_twenties
    change = payment_value - price
    if change < 0:
        return None
    elif change >= 0:
        vals = [2000, 1000, 500, 100, 25, 10, 5, 1]
        change_list = []
        for i in vals:
            if change - i >= 0:
                x = change - change%i
                y = x/i
                change_list.append(int(y))
                change = change - x
            elif change - i < 0:
                change_list.append(0)
            else:
                pass
        change_list.reverse()
        change_val = payment_value - price
        return change_list, change_val

def printWelcome():
    '''
    @brief Prints a Welcome Statement for Vendotron^TM^
    @details The printed Welcome statement includes a cheery greeting and 
             lists the prices for all drinks sold by Vendotron: Cuke, Popsi, 
             Spryte, and Dr. Pupper 
    '''
    print('''
          
          Welcome to Ventotron!! 
          Our Assorted Drinks include:
          Cuke           $1.00
          Popsi          $1.20
          Spryte         $0.85
          Dr. Pupper     $1.10
             
          Current Funds = $0.00
          ''')

# Implement FSM using a while loop and an if statement
while True:
    # will run eternally until user interupts the python kernal

    if state == 0:
    # perform state 0 operations
    # this init state, initializes the FSM itself, with all the
    # code features already set up
    
        printWelcome()
        
        ## A list representing the funds available in Vendotron: 
        # [pennies, nickels, dimes, quarters, ones, fives, tens, twenties]
        Funds = [0, 0, 0, 0, 0, 0, 0, 0]
        
        state = 1 # on the next iteration, the FSM will run state 1
    
    elif state == 1:
        # waits for keyboard entry to signify inserting a coin (0-7), 
        # buying a drink (c, p, s, d), or ejecting the remaining change (e)
        if last_key == '0':
            last_key = ''
            Funds[0] += 1
            state = 2 # s1 -> s2
        elif last_key == '1':
            last_key = ''
            Funds[1] += 1
            state = 2 # s1 -> s2
        elif last_key == '2':
            last_key = ''
            Funds[2] += 1
            state = 2 # s1 -> s2
        elif last_key == '3':
            last_key = ''
            Funds[3] += 1
            state = 2 # s1 -> s2
        elif last_key == '4':
            last_key = ''
            Funds[4] += 1
            state = 2 # s1 -> s2
        elif last_key == '5':
            last_key = ''
            Funds[5] += 1
            state = 2 # s1 -> s2
        elif last_key == '6':
            last_key = ''
            Funds[6] += 1
            state = 2 # s1 -> s2
        elif last_key == '7':
            last_key = ''
            Funds[7] += 1
            state = 2 # s1 -> s2
        elif last_key == 'c':
            last_key = ''
            ## A string representing the desired drink selected by the user
            drink = 'Cuke'
            ## An integer number of cents representing the price of the desired drink
            price = 100 # price of cuke in cents
            state = 3 # s1 -> s3
        elif last_key == 'p':
            last_key = ''
            drink = 'Popsi'
            price = 120 # price of popsi in cents
            state = 3 # s1 -> s3
        elif last_key == 's':
            last_key = ''
            drink = 'Spryte'
            price = 85 # price of spryte in cents
            state = 3 # s1 -> s3
        elif last_key == 'd':
            last_key = ''
            drink = 'Dr. Pupper'
            price = 110 # price of dr. pupper in cents
            state = 3 # s1 -> s3
        elif last_key == 'e':
            last_key = ''
            ## Variable that uses the getChange function with price = 0 to convert the change into the minimum number of denominations
            ejected_change = getChange(0, Funds) 
            Funds = ejected_change[0]
            state = 4 # s1 -> s4
        else:
            pass
            # raise SyntaxError('Invalid Keyboard Press! Press keys 0-7 to insert funds, c to vend Cuke, p to vend popsi, s to vend spryte, d to vend dr. pupper, and e to eject change without buying anything')
    
    elif state == 2:
        # calculates and displays value of Funds in dollars after the most recent coin/bill entry
        
        ## A float representing the current total value of the funds in Vendotron
        val_Funds = (1*Funds[0] + 5*Funds[1] + 10*Funds[2] + 25*Funds[3] + 100*Funds[4] + 500*Funds[5] + 1000*Funds[6] + 2000*Funds[7])/100
        print('Current Funds = $' + '{:.2f}'.format(val_Funds))
        state = 1 # s2 -> s1
    
    elif state == 3:
        # calculates value of change given the available funds and chosen drink.
        # if the available funds are sufficient, vends drink, if not,
        # error message is displayed
        
        ## Varible which calculates the change, based on the total Funds available and drink price, both in total value and a list with the least number of denominations
        change = getChange(price, Funds)
        if change == None:
            print('Insufficient Funds! ' + drink + ' costs $' + '{:.2f}'.format(price/100) + '. Please Insert more funds.')
            drink = ''
            price = None
            state = 1 # s3 -> s1
        elif change[1] >= 0:
            print('Vending ' + drink + '...')
            drink = ''
            price = None
            Funds = change[0]
            state = 4 # s3 -> s4
        else: 
            pass
        
    elif state == 4:
        # Vends the remaining change
        print('Please Take your change:' )
        print(str(Funds[0]) + ' pennies, ' + str(Funds[1]) + ' nickles, ' + str(Funds[2]) + ' dimes, ' + str(Funds[3]) + ' quarters, ' + str(Funds[4]) + ' ones, ' + str(Funds[5]) + ' fives, ' + str(Funds[6]) + ' tens, and ' + str(Funds[7]) + ' twenties ')
        print('Thank you for Using Vendotron!! Please come again!!')
        printWelcome()
        state = 1 # s4 -> s1
    else:
        # This state should not exist!
        pass
#