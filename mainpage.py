## @file mainpage.py
##
## @mainpage
##
## @section sec_intro Introduction
## This site contains the Lab projects created by Jacob Winkler for the class 
## ME 405. Jacob created this site as a portfolio 
## to showcase his work throughout the class, as he did for ME 305. Jacob also likes talking about 
## himself in the third person, as he is doing here. Although he admits this is 
## a bit weird, he hopes you will at least find his work on this site interesting 
## and/or useful! The Lab assignments and other work are listed below: \n
## \n
## \ref page_lab1 \n
## \ref page_lab2 \n
## \ref page_lab3 \n
## \ref page_lab4 \n
## \ref page_lab5 \n
## \ref page_lab6 \n
## \ref page_lab6b \n
## \ref page_lab7 \n
## \ref page_lab8 \n
## \ref page_TermProject \n
##
##
## To return back to Jacob's Portfolio index, click here: \n
## https://jcwinkle.bitbucket.io/index.html
##
## @section sec_ref References
## Jacob fully admits his mortal self would not have been able to
## create all of this code and documentation without assistance. Throughout the
## course of ME 405, Jacob worked with fellow engineers Anil Singh, Kai Quizon, and Richard Hall
## and referenced thier code when developing his own. Their code and documentation
## can be found below: \n
## \n
## Anil Singh's Website: https://asinghcp.bitbucket.io \n
## Kai Quizon's Website: https://kquizon.bitbucket.io \n
## Richard Hall's Website: https://rhall09.bitbucket.io \n
## \n
## Additionally, Jacob would like to thank his instructors, Charlie Refvem and 
## Paul Ridgley for their teachings. Their guidance was influential in writing 
## this code and generating this documentation.
##
##
## @author Jacob Winkler
##
## @date This page was created on January 8, 2021
##
## @page page_lab1 Lab 1: Vendotron Finite State Machine
## @section page_lab1_desc Description
## The code developed in this lab is a finite state machine representing the  
## Vending machine, Vendotron!! The machine sells 4 different imaginary drinks:
## Cuke ($1.00), Popsi ($1.20), Spryte ($0.85), and Dr. Pupper ($1.10). When 
## the program is run, it displays a welcome message with the drinks and their
## costs. Then it waits for the user to insert funds by pressing numbers on the
## keyboard corresponding to different denominations of money as per the key below: \n
## \n
## 0 = penny \n
## 1 = nickle \n
## 2 = dime \n
## 3 = quarter \n
## 4 = one dollar bill \n
## 5 = five dollar bill \n
## 6 = ten dollar bill \n
## 7 = twenty dollar bill \n
## \n
## Vendotron always displays the current amount of money input into the 
## machine after each virtual coin/bill is inserted. Then, the user can choose 
## the drink they want to buy by pressing letters on the keyboard according to the key below: \n
## \n
## c = Cuke \n
## p = Popsi \n
## s = Spryte \n
## d = Dr. Pupper \n
## \n 
## If there are not enough funds to pay for the drink, Vendotron displays 
## an error message with the cost of the drink. If there are enough funds, 
## Vendotron vends the drink and ejects the leftover change in the least 
## number of denominations. Additionally, the user can press the "e" key at any time and 
## Vendotron will return leftover change in the least number of denominations.
## After a successful transaction, (either buying a drink or just ejecting change),
## Vendotron displays a thank you message and then resets to display the  
## welcome message from above for the next purchase.
##
## Below is a Hand-drawn State Transition Diagram used in designing the code: \n
##
## @image html lab1_statediagram.JPG width=700px
##
## @section page_lab1_sourcecode Source Code
## Here is a link to the detailed source code: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/lab01_Vendotron.py
##
## @section page_lab1_doc Documentation
## Here is the documentation link for the Vendotron FSM: \ref lab01_Vendotron.py
##
## @page page_lab2 Lab 2: Nucleo Reaction Timer
## @section page_lab2_desc Description
## This lab uses a nucleo L476RG to create a reaction timer. It runs directly 
## on the nucleo using the PuTTY terminal. The code turns on an LED after
## 2-3 seconds (selected randomly), and waits for the user to press the blue 
## push-button on the nucleo, and records the time it took the user to react.
## It then displays the reaction time and repeats the process until the user 
## types CTRL-C to interrupt the program. It then calculates and displays the 
## average reaction time from all the recorded times from the most recent run.
##
## @section page_lab2_sourcecode Source Code
## Here is a link to the detailed source code: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/lab02_ReactionTimer.py
##
## @section page_lab2_doc Documentation
## Here is the documentation link for the Nucleo Reaction Timer: \ref lab02_ReactionTimer.py
##
## @page page_lab3 Lab 3: Nucleo Push Button Voltage Response
## @section page_lab3_desc Description
## This lab  records, plots, and generates a CSV file of the Voltage step 
## response of a predetermined push button on a Nucleo L476RG. It consists of 
## 2 files: one on the PC side that prompts data collection, plots the data, 
## and generates a CSV file, and one on the Nucleo side that waits for the 
## initialization and records the voltage values as ADC integer values as soon 
## as the button is pushed. Whenever the code is run, the PCUI prompts the user
## to start the data collection by entering "g", and it prompts the user to try
## again if they enter anything other than "g". The serial port then transmits 
## the charater to the nucleo file, which waits for the user to push the blue 
## button on the nucleo. Then, the nucleo file records 500 ADC voltage values at 
## a frequeny of 200,000 Hz and sends the Button voltage data and time data back
## to the PCUI to be plotted. The plot behaves like that of a first-order step 
## response as shown in the figure below. Both the PC file and the Nucleo file 
## run once through before stopping.
##
## @section page_lab3_plot Sample Plot
## @image html lab3_plot.png width=500px
##
## @section page_lab3_csv Sample CSV File
## Here is a link to a sample CSV file generated by the PCUI: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/lab%203%20CSVs/ADC%20Voltage%20Output%2020210203-222743.csv
##
## @section page_lab3_sourcecode Source Code
## Here is a link to the PC User Interface code: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/lab03_ButtonResponsePlot.py
##
## Here is a link to the Nucleo main file which interacts with the lab 3 PCUI: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/lab03_NucleoMain.py
##
## @section page_lab3_doc Documentation
## Here is the documentation link for the PCUI: \ref lab03_ButtonResponsePlot.py \n
## Here is the documentation link for the Nucleo main: \ref lab03_NucleoMain.py
##
## @page page_lab4 Lab 4: Temperature Data Collection on Nucleo
## @section page_lab4_desc Description
## In this lab, The nucleo L476RG is used to measure various temperatures at a given
## interval (60 seconds). The Nucleo generates a 3-column CSV file containing 
## the internal temperature of the nucleo, the ambiant room temperature, and 
## the timestamp of the temperature samples. When the file is executed on the 
## nucleo, it will run continuously until the user interrupts the program with
## CTRL-C. Then, the user can pull the CSV file from the nucleo using the 
## ampy "get" command in the command prompt. Lastly, a completely separate 
## python file run on the PC opens the CSV file and plots Nucleo Internal 
## Temperature vs. Time and Surrounding Room Temperature vs. Time, shown in 
## the plot below. 
##
## The internal temperature of the Nucleo is measured and recorded by initializing an ADC 
## object and using the read_core_temp() command. The ambiant room temperature 
## is measured using an MCP9808 temperature sensor which talks to the nucleo 
## using I2C communication. In order to effectively use I2C communication, 
## an entire MCP9808 class was created in a separate script and called by the 
## main script on the nucleo. For more details about the MCP9808 class, see the
## documentation below.
## 
## Full disclosure: I initilly misread this lab. I thought the interval had to 
## take 1 temperature data sample every 1 second instead of every 60 seconds.
## Thus I was having memory errors with the CSV at first, and when I submitted 
## this, it was far too late for me to complete a full 8-hour sample, although 
## the code should in theory work for that long. The plot and CSV file below 
## are for a temperature data set which lasted approximately 10 minutes.
##
## @section page_lab4_plot Sample Plot
## @image html lab4_plot1.png width=500px
##
## @section page_lab4_csv Sample CSV File
## Here is a link to a sample CSV file pulled from the Nucleo containing the data plotted above: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/lab%204%20CSVs/NucleoTempData10min.csv
##
## @section page_lab4_sourcecode Source Code
## Here is a link to the file containing the class written to interface with the MCP9808 and collect tmperature data\n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/mcp9808.py
##
## Here is a link to the python script used to generate the plot above: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/lab04_TempDataPlot.py
##
## Here is a link to the Nucleo main file which calls the MCP9808 class to record temperature data: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/lab04_NucleoMain.py
##
## @section page_lab4_doc Documentation
## Here is the documentation link for the PC Code which generates the plot: \ref lab04_TempDataPlot.py \n
## Here is the documentation link for the Nucleo main: \ref lab04_NucleoMain.py \n
## Here is the documentation link for the MCP9808 class: \ref mcp9808.py
##
## @page page_lab5 Lab 5: Feeling Tipsy? (Motor Controlled Balancing Platform Hand Calcs)
## @section page_lab5_desc Description
## This lab includes the hand calculations and analysis for a Nucleo-controlled,
## motor-actuated balancing platfrom that is designed to balance a ball. Below 
## is the analysis deriving the equations of motion relating the torque 
## produced by the motor to the angular acceleration of the plate and the 
## linear acceleration of the ball with respect to the plate. Note that this lab is only 
## the inital hand derivation! MATLAB models and actual mircopython code will 
## come in future labs!
##
## @section page_lab5_analysis Analysis
## @image html lab5p1.JPG width=700px
## @image html lab5p2.JPG width=700px
## @image html lab5p3.JPG width=700px
## @image html lab5p4.JPG width=700px
## @image html lab5p44.JPG width=700px
## @image html lab5p5.JPG width=700px
## @image html lab5p6.JPG width=700px
## @image html lab5p7.JPG width=700px
## @image html lab5p8.JPG width=700px
## @image html lab5p9.JPG width=700px
## The analysis above leads us to the final solution in  Matrix form:
## @image html lab5p10.JPG width=700px
##
## @page page_lab6 Lab 6: Simulation or Reality? (Pt. 1: Linearization)
## @section page_lab6_desc Description
## This lab takes the final matrix form for the balancing plate derived in lab
## 5 and linearizes it into a state space model. Then, the state space model is
## constructed in MATLAB (see the source code below) and a simulation of the 
## system is run for five cases with varying inital conditions for the ball 
## and plate system (Simulink model block diagrams shown below). Four cases 
## simulate the system as an open loop, and the fifth case adds in 
## a control loop with an arbitrary feedback gain, K. For each case,
## MATLAB generates plots of x vs time, theta vs time, xdot vs time, and 
## thetadot vs time to see how the system responds. To clarify, x refers to the horizontal
## distance of the ball from the plate CG along the plate, and theta refers to 
## the tilt angle of the plate. The linearization hand calculations, along
## with detailed plots and descriptions for each simulated case can be found below. 
##
## @section page_lab6_partner Partnership
## This portion of the analysis was constructed in collaboration with fellow 
## engineer, Rick Hall. His corresponding documentation page can be found at: \n
## https://rhall09.bitbucket.io/page_Lab6.html
##
## @section page_lab6_linearization Linearization - Hand Calcs 
## The following hand calculations linearize the matrix relationship developed 
## in lab 5 into a state space model, XDOT = A*X + B*u. It should be noted that \n 
## X = [x, theta, xdot, thetadot]' and XDOT = [xdot, thetadot, xdoubledot, thetadoubledot]'.
## 
## Starting with the lab 5 final solution:
## @image html lab5solution.JPG width=950px
## @image html lab6p1.JPG width=700px
## @image html lab6p2.JPG width=700px
## @image html lab6p3.JPG width=700px
## @image html lab6p4.JPG width=700px
## @image html lab6p5.JPG width=700px
## @image html lab6p6.JPG width=700px
##
## @page page_lab6b Lab 6: Simulation or Reality? (Pt. 2: MATLAB/Simulink Simulatons)
## @section page_lab6_sourcecode Source Code
## Here is a link to the MATLAB source code which models the state space 
## matrices derived above and generates the plots for each simulation case:\n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Sim/lab06_statespacemodel.m
##
## Here is a link to the Simulink model which simulates the four open loop cases:\n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Sim/lab06_simulink.slx
##
## @section page_lab6_OLcase1 Open Loop Case 1
## For the first open loop simulation, the ball is initially at rest on a 
## level platform directly above the center of gravity of the platform and 
## there is no torque input from the motor. The Block Diagram is shown below:
## @image html lab06c123_bd.JPG width=700px
##
## Here are the response plots of x, theta, xdot, and thetadot for a 0.4 second simulation:
## @image html lab06case1.png width=700px
## As can be seen in all of the plots above, the modeled system has no responses
## when all the initial conditions are zero. This runs as expected, since when the ball 
## is perfectly balanced over the plate CG and there is no initial ball velocity,
## xdot, or tilt angle, theta, the system should not move. 
##
## @section page_lab6_OLcase2 Open Loop Case 2
## For the second open loop simulation, the ball is initially at rest on
## a level platform offset horizontally from the center of gravity of the 
## platform by 5 cm and there is no torque input from the motor. The Block 
## Diagram looks identical to the case 1 diagram (shown above), but has these
## different initial conditions in the state space block.
##
## Here are the response plots of x, theta, xdot, and thetadot for a 0.4 second simulation:
## @image html lab06case2.png width=700px
## As can be seen in the plots above, x and xdot initially decrease 
## (the ball moves closer to the center of the plate) as theta and thetadot 
## increase. After about 0.2 seconds however, x and xdot begin to rapidly 
## increase as theta and thetadot continue to increase. It sould be noted that 
## when deriving our model, the small angle approximation was used, which 
## means that any data points where theta > 10 degrees [0.175 radians] are 
## inaccurate and should be disregarded.
##
## @section page_lab6_OLcase3 Open Loop Case 3
## For the third open loop simulation, the ball is initially at rest on a 
## platform inclined at 5 degrees directly above the center of gravity of the 
## platform and there is no torque input from the motor.  The Block Diagram 
## looks identical to the case 1 diagram (shown above), but has these 
## different initial conditions in the state space block.
##
## Here are the response plots of x, theta, xdot, and thetadot for a 0.4 second simulation:
## @image html lab06case3.png width=700px
## As can be seen in the plots above, x and xdot initially increase as expected
## (the ball moves away from the center of the plate in the positive x 
## direction) as theta and thetadot 
## increase. After about 0.2 seconds however, x and xdot begin to rapidly 
## increase way more than expected as theta and thetadot continue to increase. 
## At this time, theta becomes larger than 10 degrees [0.175 radians], meaning
## that the small angle approximation becomes invalid, so the response after 
## 0.2 seconds is inaccurate.
## 
## @section page_lab6_OLcase4 Open Loop Case 4
## For the fourth open loop simulation, the ball is initially at rest on a 
## level platform directly above the center of gravity of the platform and 
## there is an impulse of 1 mNm·s applied by the motor. The Block Diagram is shown below:
## @image html lab06c4_bd.JPG width=700px
##
## Here are the response plots of x, theta, xdot, and thetadot for a 0.4 second simulation:
## @image html lab06case4.png width=700px
## As can be seen in the plots above, x and xdot overall decrease 
## (the ball moves away from the center of the plate in the negative x 
## direction) as theta and thetadot decrease. The initial increase in the x 
## plot is unexpected and may be due to how the impulse was implemented in Simulink.
##
## @section page_lab6_CLcase Closed Loop Case
## For the closed loop simulation, the ball is initially at rest on
## a level platform offset horizontally from the center of gravity of the 
## platform by 5 cm and there is no torque input from the motor. The closed 
## negative feedback loop follows u = -K*X, where -K*X represents the feedback.
## The Gain matrix, K, is shown below:
## @image html lab06_K.JPG width=500px
##
## The closed loop Block Diagram is shown below:
## @image html lab06closedloop_bd.JPG width=700px
## 
## Here are the response plots of x, theta, xdot, and thetadot for a 20 second simulation:
## @image html lab06case4closedloop.png width=700px
## As can be seen in the plots above, all of the plots initially oscillate, 
## but eventually approach a steady state value of zero. Once all of the plots 
## reach 0 after about 15 seconds, the system reaches equilibrium, which is 
## expected from a closed loop controller. 
##
## @page page_lab7 Lab 7: Feeling Touchy?
## @section page_lab7_desc Description
## This lab contains just one python script with both the TouchPanel class 
## for a resistive touch panel 
## as well as sample code to be run directly on the nucleo in the PuTTY 
## terminal which implements the various functions within the class. When the 
## sample code is run, Touchy the Touch Panel greets you and prompts you to 
## start touching it, making the whole situation awkward. If, as per the lab 
## title, you start "Feeling Touchy", you can have Touchy tell you the 
## x-distance, y-distance, or both x and y distance from the its center [all 
## dims in millimeters]. For detailed descriptions of the class, its 
## functions, and the test code, see the source code and documentation links 
## below. 
##
## @section page_lab7_sourcecode Source Code
## Here is a link to the detailed source code of the TouchPanel.py file: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Files/TouchPanel.py
##
## @section page_lab7_doc Documentation
## Here is the documentation link for the file containing the TouchPanel class:
## \ref TouchPanel.py \n
##
## @page page_lab8 Lab 8: Motordriver and Encoder classes
## @section page_lab8_desc Description
## This lab contains the two classes which will be vital in actuating the 
## balancing platform: the EncoderDriver class and the MotorDriver class. 
##
## The EncoderDriver class can record the value of each encoder that is hooked up to 
## a motor, which corresponds to the position of the motor shaft. The encoder 
## class also can update, set, and zero the encoder position, as well as find 
## the difference between the current encoder position and the previous one 
## (which can be helpful in determining motor velocity). 
##
## The MotorDriver class can actually actuate the motors by setting the duty 
## cycle of the motor in terms of a pulse width percentage of its maximum 
## speed. when calling the set_duty method from this class, the user specifies
## a value ranging from -100 to 100, where -100 spins the motor at its max 
## speed backwards, 100 spins it at its max speed forward, and 0 stops it. 
## This class also contains methods to disable and enable the motor for safety. 
## test code waas developed to ensure that the motors actually do spin when 
## the duty cycle is set. This test code employs an interupt which disables 
## the motors in the case of a motor fault (current overflow) condition. In the
## rare occasion that the motors experience a fault, the built-in nFault pin on
## the Nucleo will turn low, calling an ISR which disables the motors and 
## triggers a Motor fault User Interface. The user is then prompted to fix 
## their hardware and type "Fault Cleared" to resume their motor
##
## It should be noted that the class files in this lab were edited and further
## developed from files created in a previous class: ME 305. Here is a link to
## Jacob's ME 305 repository: \n 
## https://jcwinkle.bitbucket.io/ME305/index.html
##
## @section page_lab8_partner Partnership
## This lab was completed in collaboration with fellow engineer, Rick Hall. 
## His documentation portfolio can be found at: \n
## https://rhall09.bitbucket.io
##
## However, it should be noted that for this lab, we both just pushed to 
## Jacob's repository (source code linked below).
##
## @section page_lab8_sourcecode Source Code
## Here is a link to the file containing the EncoderDriver class: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Files/EncoderDriver.py \n
## And Here is a link to the file containing the MotorDriver class and test code: \n
## https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Files/MotorDriver.py \n
##
## @section page_lab8_doc Documentation
## Here are the documentation links for both classes:\n
## \ref EncoderDriver.py \n
## \ref MotorDriver.py \n
##
## @page page_TermProject Term Project - Balancing Table
## @section page_TermProject_desc Description
## The goal of this project is to balance a ball on a plate which is free to 
## rotate in two dimensions. To do this, the position of the ball on the plate 
## is read using a resistive touch panel and the TouchPanel class developed in 
## \ref page_lab7, angular position and velocity of the table is read from 
## encoders using the EncoderDriver class developed in \ref page_lab8, and 
## motors (one controlling the X-tilt of the plate and one controlling the Y-tilt) 
## are actuated using the MotorDriver class, also developed in lab 8. 
## However, in order to actuate the motors with the appropriate duty cycle to 
## balance a ball, proper feedback gains for each system variable (table angle, 
## table angular velocity, ball position, and ball velocity) must be used. 
##
## To theoretically calculate the gains to control the balancing plate system, 
## a MATLAB script was created. Inputting dimensions from the system and the 
## initial hand calculations from \ref page_lab5, the state space matrices are 
## defined, and the feedback loop gains are symbolically calculated for each 
## state space variable based on u = -K*x. From there, the closed loop gains 
## are compared to an ideal system with a desired damping ratio and natural 
## frequency. The system gains are then computed using polynomial matching 
## between the transfer function coefficients of the actual system 
## (solved symbolically) and the coefficients of ideal system transfer 
## function. See the source code section below for a link to the MATLAB script. 
##
##  Implementing these gains into a micropython script, 6 main methods were 
##  created to control the board. The Xtable and Ytable methods use the system 
##  variables (read using the touch panel and encoder classes described below) 
##  and gains (hand-tuned based on the ones generated in the MATLAB script) to 
##  calculate the appropriate duty cycle to apply to each motor to balance the 
##  ball in the center of the plate. The touch panel method reads the X position, 
##  X velocity, Y position, and Y velocity of the ball on the plate. The encoder 
##  method reads the angle and rotational speed of the X and Y motor shafts and 
##  converts it plate angle and angular velocity in both the X and Y directions. 
##  Alternatively, another method that calls an IMU to measure plate orientation 
##  could be used instead of the encoder task. To switch between these methods, 
##  the initialization variable named EI in the controller code must be switched 
##  (EI = 0 runs the encoder method and EI = 1 runs the IMU method). Lastly, the 
##  CSV writer method writes the all of data read by the methods described above 
##  to a CSV file for later plotting and analysis. 
##
##  To operate these task methods cooperatively, a cooperative-multitasking script
##  developed by Professor Ridgely was utilized. The multitasking script operates 
##  each of the above tasks at pre-set periods, running the tasks according to a
##  set priority. These tasks are defined in the initialization portion of our 
##  script, then operated with the Professor Ridgely's scheduler in a 'while True:' 
##  loop. The motor fault funcitonality is also operated in this loop.
##
##  @section page_TermProject_results Results
##  With a great deal of iterative tuning of the system, the table was able to 
##  balance itself in both directions with relative ease and balance the ball 
##  for small deflections. Tuning of the saturation limits of the duty cycle of 
##  the motor helped to limit the occurrence of motor faults, however they still 
##  occur for large table deflections. Large ball deflections tend to introduce 
##  instability into the system that inevitably results in a motor fault. A plot 
##  of the system behavior is shown below:
##
##  @image html TermProject_Plot.jpg width=700px
##
##  As can be seen in the plot, the table returns to a stable orientation after 
##  some oscillation for relatively small table displacements. Once the ball is 
##  introduced, the table manages to stop the motion of the ball but not return 
##  it perfectly to the center.
##
##  The resolution of the data is limited by the mechanics of the data collection 
##  task. As the majority of time spent on this project was dedicated to improving 
##  system stability, the data collection system has not been refined and only 
##  collects data every quarter-second. As such, it is sufficient to display general 
##  trends and little more. 
##
##  It is also noteworthy that the ability of the system to respond to large motions 
##  is limited due to the faulting of the motor. A saturation limit is implemented 
##  in the script to limit the duty cycle to a level that will not cause near-continuous 
##  motor faulting, iteratively refined to an optimal limit of 60%. This duty cycle 
##  provides sufficient torque to run the system, if only barely. Additionally, 
##  the torque provided below approximately 40% duty cycle is not sufficient to 
##  affect the system at all. As such, the motors operate in a relatively narrow 
##  band of torques, resulting in a system non-linearity that is not accounted for 
##  in our theoretical model. 
##
##  Due to the nonlinearities in the model and other inconsistancies in the comparison 
##  between reality and theoritical predictions, each gain value had to be tuned 
##  to provide a reasonable response. However, the tuning process never allowed 
##  the system to operate at angles larger than approximately 10 degrees without 
##  becoming unstable. 
##
##  @section page_TermProject_usermanual User Manual
##  To run the balancing table, first ensure all the necessary files are on the 
##  nucleo. The main file may be run with the 'execfile()' funciton or renamed 
##  'main.py.' In either case, the script will send a prompt to the serial terminal 
##  prior to activating the motors. If encoder angle tracking is enabled, the 
##  user will be prompted to level the table in order to zero the encoders at the 
##  correct position.
##
##  Once the program is in operation, it will attempt to return the table to the 
##  level position. For minimal deflections and the ball remaining close to the 
##  center the script will run as intended. For larger deviations, however, the 
##  motors may enter a situation where the overcurrent fault circut trips. In this 
##  circumstance, an external interrupt is triggered on the board that stops the 
##  motors. The user will be prompted to type 'Fault Cleared' into the serial 
##  terminal in order to reset the table. This is to ensure that the user manually 
##  shifts the table to clear it from the faulted position prior to resuming 
##  operation.
##
##  Additional parameters that may need to be adjusted cannot be modified through 
##  the terminal and may be manually editied in the script. These should only be 
##  modified by users who are familiar with the project scenario and this script 
##  in particular. The first four parameters are boolean flags that 
##  exist in the 'if __name__ == '__main__' section of the script. the first two,
##  'Rx' and 'Ry', which enable the controllers for each axis of the table, labeled 
##  according to the corresponding touch panel axis. The third toggles between 
##  encoder-based angle measurement and inertial measurement unit (IMU) angle measurement, 
##  labeled 'EI'. The IMU is enabled when this boolean is 1 and the encoders are 
##  enabled when it is 0. The final boolean, 'CD' enables the collection of data 
##  from the sensors, stored in a corresponding CSV file. The remaining adjustable 
##  parameters are controller gains, housed in the controller methods near the 
##  beginning of the script. These gains may be tuned as necessary for individual 
##  hardware sets, as optimal gains tend to be hardware specific. Unfortunately, 
##  not all hardware is created equal... 
##
##  If data collection is enabled, wait until the terminal indicates that collection 
##  has begun before placing the ball on the touch panel. This will ensure that 
##  the system response is captured. The data is stored in a CSV file, which may 
##  be collected from the board by a program such as ampy. \n
##  To end the program, press ctl+c. This will disable the motors and the motor 
##  interrupt, then indicate that the program is complete. \n 
##
##  @section page_TermProject_partnership Partnership
##  This lab was completed in collaboration with fellow engineer, Rick Hall. 
##  His corresponding documentation page can be found at: \n
##
##  https://rhall09.bitbucket.io/page_TermProject.html
##
##  However, it should be noted that for this lab, we both just pushed to 
##  Jacob's repository (source code linked below). \n
## 
##  @section page_TermProject_references References
##  This script utilizes code from a number of sources: \n \n
##
##  1. Cooperative Multitasking Software, by Professor JR Ridgely \n\n
##  This set of classes was utilized to provide timing for the tasks in our script. 
##  It was developed by Professor JR Ridgely as a scheduling file example for ME405, 
##  and has been minimally modified for use in our script. His code can be found at 
##  the link below: \n
##  https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Files/scheduler_files/
##  \n\n\n
##  2. BNO055 Inertial Measurement Unit Drivers, from Adafruit Industries \n\n
##  This hardware driver was utilized in our script to expedite the use of the 
##  Adafruit BNO055 IMU. It was developed by Radomir Dopieralski for Adafruit 
##  Industries and extended for MicroPython by Peter Hinch. This code is stored 
##  in our repository at the link below: \n
##  https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Files/BNO055%20IMU/
##  \n
##  The original source may be found at the github link below: \n
##  https://github.com/micropython-IMU/micropython-bno055
##  \n\n\n
##  3. Touch Panel, Motor, and Encoder drivers from previous labs: \n\n
##  Drivers developed in earlier ME405 labs by Rick Hall and/or Jacob Winkler were 
##  used to run the corresponding hardware. The drivers can be found at the following 
##  links: \n\n
##  \ref page_lab7 \n
##  \ref TouchPanel - https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Files/TouchPanel.py
##  \n\n
##  \ref page_lab8 \n
##  \ref MotorDriver - https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Files/MotorDriver.py
##  \n
##  \ref EncoderDriver - https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Files/EncoderDriver.py
##
##  @section page_TermProject_sourcecode Source Code
##  The source code for the main script can be found in the link below: \n
##  https://bitbucket.org/jcwinkle/me405_repository/src/master/Final%20Project%20Files/projectFile.py
##
##  @section page_TermProject_documentation Documentation
##  The main script documentation is included at the link below: \n
##      - Term Project Main Script:       \ref projectFile.py

