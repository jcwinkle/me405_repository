'''
@file lab04_MCU_temp_test.py
@brief File containing sample code which measures the internal temperature of the Nucleo
@details Takes 10 temperature data samples at 1 second intervals from the 
         nucleo and records them into a CSV. This is merely test code.
@author Jacob Winkler
@date February 3, 2021
'''
import pyb
import utime

adcall = pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels
temp_list = []
time_stamp = []

while True:
    if len(temp_list) < 10:
        temp_list.append(adcall.read_core_temp())
        time_stamp.append(utime.ticks_ms)
        # sleep for 1 second in between data samples
        utime.sleep_ms(1000)
    else:
        # converts list of timestamps to list of times starting at t = 0 when 
        # the first data point was taken.
        time_list = [value-time_stamp[0] for value in time_stamp]
        break
    
with open ("lab04_internaltemp.xlsx","w") as csv_file:
    for i in range(len(time_list)):
        csv_file.writerow([time_list[i], temp_list[i]]) 
print ("The file has by now automatically been closed.")