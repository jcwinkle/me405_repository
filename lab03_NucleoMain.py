'''
@file lab03_NucleoMain.py
@brief File on nulceo which collects ADC data to record the push-button response
@details This code recieves "G" from lab03_ButtonResponsePlot.py and records 
         ADC data until the button is pushed and released. Then the code stops 
         collecting data and sends the data back to the PC to be plotted.
@author Jacob Winkler
@date February 3, 2021
'''

from pyb import UART
import array
import pyb 

# enables interrupts
pyb.enable_irq

# Variable inititalizing the UART serial port
uart = UART(2)

## frequency at which the timer triggers, in Hz
sample_freq = 200000

## number of samples of ADC integers taken 
sample_range = 500

## Array containing the ADC integers that represent the Button Voltage values 
# (Range 0-4095)
button_data = array.array ('H', (0 for i in range(sample_range)))

## Array containing the times, in milliseconds, that correspond to each ADC 
# voltage value in the button_data array 
time_data = array.array('H', (int(i*(sample_range/sample_freq)*1e3) for i in range(sample_range)))

## Array which ensures that relevent data is recorded into the button_data 
# array
buffer = array.array ('H', (0 for index in range(20)))

# Push Button Pin, in this case Pin C0, which corresponds to ADC Pin A5
Button = pyb.Pin(pyb.Pin.board.PC0, mode=pyb.Pin.IN)

# variable that creates an ADC on desired Button pin, pin A5 in this case
adc = pyb.ADC(Button)

# timer that triggers at a frequency defined by sample_freq                                     
tim6 = pyb.Timer(6, freq=sample_freq) 

# variable that represents whether the button has been pushed on the Nucleo
flag = None

def button_press(Line):
    '''
    @brief This function calls when the blue button on the Nucleo is pressed and turns the variable flag to 'True' (Boolean 1)
    @param Line This param allows for functionality of the interrupt service request.
    '''
    global flag
    global val
    if val == 103:
        flag = 1
    
## Initializes an external interrupt on the falling edge for Pin C13 
extint = pyb.ExtInt(pyb.Pin.cpu.C13,
                    pyb.ExtInt.IRQ_FALLING, 
                    pyb.Pin.PULL_NONE, 
                    callback = button_press)

while True:
    if uart.any() != 0:
        # character(s) read from the serial port
        val = uart.readchar()
    elif flag == 1:
        val = None
        # this code will continue to run until the buffer array begins to have
        # non-negligable values (In other words, when the pin voltage starts 
        # changing). this loop only breaks once an ADC integer value greater 
        # than 20 is returned
        adc.read_timed(buffer, tim6)
        if buffer[-1] >= 20:
            break
        
# reads the pin ADC interger data as the pin voltage changes            
adc.read_timed(button_data, tim6) 
uart.write(str(button_data) + '  ' + str(time_data)+ '\n')
