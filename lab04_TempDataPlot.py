'''
@file lab04_TempDataPlot.py
@brief File on the PC which opens the Temperature data CSV file and plots it
@details This code runs on the PC in the spyder terminal and opens the 
         prerecorded temperature data CSV file pulled frrom the Nucleo which 
         contains internal and ambiant temperature samples taken every 60 seconds 
         for a predetermined long amunt of time. It then records the data into 
         lists stored loacally in the program and plots Internal Nucleo 
         Temperature vs. Time and Ambiant Room Temperature vs. Time in separate
         subplots.
@author Jacob Winkler
@date February 10, 2021
'''
from matplotlib import pyplot as plt

## list of timestamps of each temperature sample (minutes)
time = []

## list of recorded internal nucleo temperature values (degrees Celcius)
IntTemp = []

## list of recorded ambiant room temperature values (degrees Celcius)
AmbTemp = []

## Variable that opens the Nucleo Temprature Data CSV file. 
data = open('NucleoTempData.csv');

# Read data indefinitely. Loop will break out when the file is done.
while True:
    ## Variable that reads a line of data. 
    # Each line is formatted to be (time, internal temp, ambiant temp)
    line = data.readline()
    
    # If the line is empty, there are no more rows so exit the loop
    if line == '':
        break
    
    # If the line is not empty, strip special characters, split on commas, and
    # then append each value to its list.
    else:
        (t, Int, Amb) = line.strip().split(',');
        time.append(t)   
        IntTemp.append(Int)
        AmbTemp.append(Amb)

# must pop the first element in each list to remove the header of each column
# of the CSV
time.pop(0)
time = [float(i)/60 for i in time] # must divide by 60 to convert seconds to minutes
IntTemp.pop(0)
IntTemp = [float(i) for i in IntTemp]
AmbTemp.pop(0)
AmbTemp = [float(i) for i in AmbTemp]

# calulate averages of temp lists to format plots better
avgInt = sum(IntTemp)/len(IntTemp)
avgAmb = sum(AmbTemp)/len(AmbTemp)

# closes CSV file
data.close()

# To make the example complete, plot the data as a subplot.
plt.figure(1)

# Nucleo Internal Temperature Plot
plt.subplot(2,1,1)
plt.title('Nucleo Internal Temperature vs. Time')
plt.plot(time, IntTemp)
plt.xlabel('Time [min]')
plt.ylabel('Core Temp [deg C]')
plt.ylim(round(avgInt-2), round(avgInt+2))

# Ambiant Room Temperature Plot
plt.subplot(2,1,2)
plt.title('Surrounding Room Temperature vs. Time')
plt.plot(time, AmbTemp)
plt.xlabel('Time [min]')
plt.ylabel('Room Temp [deg C]')
plt.ylim(round(avgAmb-2), round(avgAmb+2))

plt.subplots_adjust(hspace =0.75)