# Import any required modules
import pyb
import micropython

# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)

## Pin object to use for Blinking LED. Attached to PA5
MyPin = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

## Callback function to run when timer overflows
def MyCallback(timSource):
    print(timSource.counter())
    MyPin.value(1-MyPin.value())
    
## Create a timer object (timer 6) in general purpose counting mode
# at freq = 10 Hz
MyTimer = pyb.Timer(6, freq=10, callback=MyCallback)

# Main program loop
while True:
    print('In while loop')
    pyb.delay(50)   # should make print statement happen at 20 Hz
    