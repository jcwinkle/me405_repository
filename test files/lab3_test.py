from pyb import UART
import array
import pyb 

## Variable inititalizing the UART serial port
uart = UART(2)

## frequency at which the timer triggers, in Hz
sample_freq = 200000

## number of samples of ADC integers taken 
sample_range = 200

## Array containing the ADC integers that represent the Button Voltage values 
# (Range 0-4095)
button_data = array.array ('H', (0 for index in range (sample_range)))

## Array containing the times, in milliseconds, that correspond to each ADC 
# voltage value in the button_data array 
time_data = array.array('H', (int(i*(sample_range/sample_freq)*1e3) for i in range(sample_range)))

while True:
    if uart.any() != 0:
        ## character(s) read from the serial port
        val = uart.readchar()
        if val == 103:
            break
        
uart.write(str(button_data) + '  ' + str(time_data))