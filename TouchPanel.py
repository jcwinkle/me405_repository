'''
@file TouchPanel.py
@brief File containing the class for a resistive touch panel 
@details This file contains the TouchPanel class for a resistive touch panel, 
         as well as sample code to be run directly on the nucleo in the PuTTY 
         terminal which implements the various functions within
         the class. When run, the sample code prompts the user to choose the 
         how the touch panel should sample data: horizontally, which returns 
         the x-distance (in millimeters) from the center of the plate, 
         vertically, which returns the y-distance (in millimeters) from the 
         center of the plate, or all directions, which returns the x and y 
         distances from the center of the plate along with the z-value (in 
         ADC_volts). Data samples are take from the touch panel approximately 
         every 100 ms, or 0.1 seconds. The code runs continuously, prompting 
         the user after each sampling session is done to select another one. 
         To end each sampling session, the user must press CTRL-C. To exit the
         program, the user must press CTRL-C when prompted to select another test.
@author Jacob Winkler
@date March 3, 2021
'''

import pyb
import utime
import array
 

class TouchPanel:
    '''
    @brief Class which establishes and reads sensor data from a physical resistive touch panel. 
    @details This class employs various functions to record the position at 
             which an object is touching the touch panel. The x_read function 
             employs a voltage divider between the positive X pin and negative 
             X pin to calculate the distance along the x-axis of the panel. 
             The y_read function employs a voltage divider between the positive 
             Y pin and negative Y pin to calculate the distance along the 
             y-axis of the panel. The z_read function employs a voltage 
             divider between the positive X pin and positive Y pin to sense 
             whether an object is touching the panel; if the ADC value is 
             somewhere between high and low, then an object is touching the 
             panel. Lastly, the coord_read function calls the x_read, y_read, 
             and z_read functions to give a cartesian coordinate location of 
             where the object is touching the panel. 
    '''
    
    def __init__(self, xm, xp, ym, yp): 
        '''
        @brief Initialization function for the TouchPanel class
        @param xm the pin object connected to the negative X side of the touch panel
        @param xp the pin object connected to the positive X side of the touch panel
        @param ym the pin object connected to the negative Y side of the touch panel
        @param yp the pin object connected to the positive Y side of the touch panel
        '''
        # initialize array of x, y, z values of touch panel 
        self.coord = array.array('f',[0, 0, 0])
        self.touch = False
        
        # initialize buffer arrays for filtering data
        self.x_buffer = array.array('h',[0, 0, 0])
        self.y_buffer = array.array('h',[0, 0, 0])
        self.z_buffer = array.array('h',[0, 0, 0])
        
        # define pins according to input parameters
        self.xm = pyb.Pin(xm)     # Pin PA1 on PCB 
        self.xp = pyb.Pin(xp)     # Pin PA7 on PCB
        self.ym = pyb.Pin(ym)     # Pin PA0 on PCB
        self.yp = pyb.Pin(yp)     # Pin PA6 on PCB  
        
        # initialize pins
        self.pin_high = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
        self.pin_high.high()
        self.pin_low  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
        self.pin_low.low()
        self.pin_float = pyb.Pin(self.ym, mode=pyb.Pin.IN)
        self.pin_read = pyb.ADC(self.xm)
        
        # Since reassigning pins causes delays in running the code, the 
        # last_call variable tracks the previous read function(s) called 
        # in order to optimize code efficiency: 
        # If x_read was last called, last_call = 1
        # If y_read was last called, last_call = 2
        # If z_read, then x_read were just called, last_call = 3
        # If z_read, then y_read were just called, last_call = 4
        
        # The initial pin conditions above set up the input pins as if z_read,
        # then y_read had just been called, so last_call = 4
        self.last_call = 4
     
        
    def x_read(self):
        '''
        @brief Reads x-location on the touch panel relative to the center 
        @details To scan the X component, the resistor divider between xp and 
                 xm must be energized. To do so, the pin connected to xp must
                 be configured as a push-pull output and asserted high and the 
                 pin connected to xm must be configured as a push-pull output 
                 and asserted low. Then, the voltage at the center of the 
                 divider can be measured by floating yp and measuring the 
                 voltage on ym. This voltage is then converted to the 
                 x-distance from the center of the panel, in millimeters.
        '''
        
        # the below code optimally reassigns pins for x-read
        if self.last_call == 2 or self.last_call == 4:
        # If the last read functions were z_read, then y_read, or was just 
        # y_read, configure pins such that ADC & Low already set:
            
            self.pin_high = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.pin_high.high()
            self.pin_low  = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            self.pin_low.low()
            self.pin_float = pyb.Pin(self.yp, mode=pyb.Pin.IN)
            self.pin_read = pyb.ADC(self.ym)
            self.last_call = 1
            
        elif self.last_call == 3:
        # If the last read functions were z_read, then x_read, configure pins 
        # such that ADC & Low already set:

            self.pin_high = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.pin_high.high()
            # self.pin_low  = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            # self.pin_low.low()
            self.pin_float = pyb.Pin(self.yp, mode=pyb.Pin.IN)
            # self.pin_read = pyb.ADC(self.ym)
            self.last_call = 1            
        
        # Filter by taking 3 data points and averaging to reduce noise:
        utime.sleep_us(3)
        self.x_buffer[0] = self.pin_read.read()
        utime.sleep_us(3)
        self.x_buffer[1] = self.pin_read.read()
        utime.sleep_us(3)
        self.x_buffer[2] = self.pin_read.read()
        
        # when taking the average, 2048 is subtracted from the ADC_volt 
        # reading to get the x-distance from the center of the plate. This 
        # distance is then converted to mm by multiplying by 0.0464 due to the
        # geometry of the touch panel. 
        self.coord[0] = 0.0464*(round((self.x_buffer[0]+self.x_buffer[1]+self.x_buffer[2])/3)-2048)
    
    
    def y_read(self):
        '''
        @brief Reads y-location on the touch panel relative to the center
        @details To scan the Y component, the resistor divider between yp and 
                 ym must be energized. To do so, the pin connected to yp must 
                 be configured as a push-pull output and asserted high and the 
                 pin connected to ym must be configured as a push-pull output 
                 and asserted low. Then, the voltage at the center of the 
                 divider can be measured by floating xp and measuring the 
                 voltage on xm. This voltage is then converted to the 
                 y-distance from the center of the panel, in millimeters.
        '''
        
        # the below code optimally reassigns pins for y-read 
        if self.last_call == 1 or self.last_call == 3:
        # If the last read functions were z_read, then x_read, or was just 
        # x_read configure pins such that ADC & Low already set:
                
            self.pin_high = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.pin_high.high()
            self.pin_low  = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            self.pin_low.low()
            self.pin_float = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            self.pin_read = pyb.ADC(self.xm)
            self.last_call = 2
            
        elif self.last_call == 4:
        # If the last read functions were z_read, then y_read, configure pins 
        # such that ADC & Low already set:
                            
            # self.pin_high = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            # self.pin_high.high()
            self.pin_low  = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            self.pin_low.low()
            self.pin_float = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            # self.pin_read = pyb.ADC(self.xm)
            self.last_call = 2

        
        # Filter by taking 3 data points and averaging to reduce noise:
        utime.sleep_us(3)
        self.y_buffer[0] = self.pin_read.read()
        utime.sleep_us(3)
        self.y_buffer[1] = self.pin_read.read()
        utime.sleep_us(3)
        self.y_buffer[2] = self.pin_read.read()
        
        # when taking the average, 2048 is subtracted from the ADC_volt 
        # reading to get the y-distance from the center of the plate. This 
        # distance is then converted to mm by multiplying by -0.0269 due to 
        # the geometry of the touch panel.
        self.coord[1] = 0.0269*(round((self.y_buffer[0]+self.y_buffer[1]+self.y_buffer[2])/3)-2048)
    
    
    def z_read(self):
        '''
        @brief Reads z-value of the touch panel- determines if an object is touching the panel or not 
        @details To scan the Z component, the voltage at the center node must 
                 be measured while both resistor dividers are energized. To 
                 achieve this, the pin connected to yp is configured
                 as a pushpull output asserted high while the pin connected to 
                 xp is configured as a push-pull output and asserted low. Then 
                 the voltage on one of the remaining pins, either xm or ym, is 
                 measured to determine if there is contact with the screen.
                 In the case of no-contact the ym pin reads high, as it is 
                 effectively pulled high to the value of yp. Similarly, 
                 xm would read low as it is effectively pulled low to the value 
                 of xp. Thus, if the voltages measured on either xm or ym read 
                 between high and low, then there is contact with the screen.
        '''
        
        # the below code optimally reassigns pins for z-read 
        if self.last_call == 2:
        # If the last read function was y_read, configure pins such 
        # that ADC & Low already set:
                
            # self.pin_high = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            # self.pin_high.high()
            self.pin_low  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.pin_low.low()
            self.pin_float = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            # self.pin_read = pyb.ADC(self.xm)
            self.last_call = 4
            
        elif self.last_call == 1:
        # If the last read function was x_read, configure pins such 
        # that ADC & Low already set:
                            
            self.pin_high = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.pin_high.high()
            self.pin_low  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.pin_low.low()
            self.pin_float = pyb.Pin(self.xm, mode=pyb.Pin.IN)
            # self.pin_read = pyb.ADC(self.ym)
            self.last_call = 3
        
        # Filter by taking 3 data points and averaging to reduce noise:
        utime.sleep_us(3)
        self.z_buffer[0] = self.pin_read.read()
        utime.sleep_us(3)
        self.z_buffer[1] = self.pin_read.read()
        utime.sleep_us(3)
        self.z_buffer[2] = self.pin_read.read()
        self.coord[2] = round((self.z_buffer[0]+self.z_buffer[1]+self.z_buffer[2])/3)
        
        # If ADC reads less than high (defined as >4080 ADC_volts) 
        # or greater than low (defined as <16 ADC_volts), a touch is detected:
        if 16 < self.coord[2] < 4080:
            self.touch = True    
        else:
            self.touch = False
            
    
    def coord_read(self):
        '''
        @brief Reads all 3 directions of the touch panel to give cartesian coordinate location
        @details calls z_read to sense if an object is touching the panel, 
                 and then only calls x_read and y_read if there is an object 
                 touching the panel. This yields coordinates (x, y) in 
                 millimeters relative to the center of the plate of where the 
                 object is touching the panel.
        ''' 
        # the below code calls z_read until a touch is detected
        if self.touch == False:
            self.z_read()
            
        else:
            # the below code ensures the 3 different read functions are read in 
            # the most efficient order due to pin reassignment 
            if self.last_call == 1:
                self.x_read()
                self.z_read()
                self.y_read()
                
            else:
                self.y_read()
                self.z_read()
                self.x_read()
            
            
if __name__ == '__main__':
    while True:
        try:
            # define touch panel with local pins on PCB
            touchsensor = TouchPanel('PA1','PA7','PA0','PA6')
            
            # prompt user to specify touch panel measurement: x, y, or coordinates
            touch_direction = input('\n'*20 + '''Hey There...\nMy name's Touchy, the Touch Panel\nHow do you want to touch me today?\n     -To touch me horizontally (return x-values), press "x"\n     -To touch me vertically (return y-values), press "y"\n     -To touch me all over (return x, y, and z-values), enter "all"\n\n     Enter choice here: ''')
            if touch_direction != 'x' and touch_direction != 'y' and touch_direction != 'all':
                touch_direction = 'all'
            
            if touch_direction == 'x':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        touchsensor.x_read()
                        t1 = utime.ticks_us()
                        delta_t = utime.ticks_diff(t1,t0)
                        print('\n'*20 +'Oh! You are touching me at X = ' + str(touchsensor.coord[0]) + 'mm from the center!\n')
                        print('Touch data collection time: ' + str(delta_t) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                        
                    except KeyboardInterrupt:
                        break 
                    
            elif touch_direction == 'y':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        touchsensor.y_read()
                        t1 = utime.ticks_us()
                        delta_t = utime.ticks_diff(t1,t0)
                        print('\n'*20 +'Ah! You are touching me at Y = ' + str(touchsensor.coord[1]) + 'mm from the center\n')
                        print('Touch data collection time: ' + str(delta_t) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                        
                    except KeyboardInterrupt:
                        break
                    
            elif touch_direction == 'all':
                while True:
                    try:
                        if touchsensor.touch == False:
                            t0 = utime.ticks_us()
                            touchsensor.z_read()
                            t1 = utime.ticks_us()
                            delta_t = utime.ticks_diff(t1,t0)
                            print('\n'*20)
                            print('\n' * 2 + '''Aw, why aren't you touching me?\n''')
                            print('Data collection time: ' + str(delta_t) + ' us' + '\n'*5)
                            
                        elif touchsensor.touch == True:
                            t0 = utime.ticks_us()
                            touchsensor.coord_read()
                            t1 = utime.ticks_us()
                            delta_t = utime.ticks_diff(t1,t0)
                            print('\n'*20)
                            print('\n'*2 +'Oh yeah! You are touching me at: \nX = ' + str(touchsensor.coord[0]) +'mm, Y = ' + str(touchsensor.coord[1]) + 'mm from the center, with Z = ' + str(touchsensor.coord[2]) + ' \n')
                            print('Touch data collection time: ' + str(delta_t) + ' us' + '\n'*5)
                            
                        utime.sleep_ms(100)
                        
                    except KeyboardInterrupt:
                        break
                    
        except KeyboardInterrupt:
            print('\n'*50 + 'Ah, that was some good touching...\nCome back to touch more soon!!! ;) \n\n')
            break