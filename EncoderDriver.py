'''
@file EncoderDriver.py
@brief File defining a generic encoder class
@author Jacob Winkler
@date March 4, 2021 
'''
import pyb
import utime
import array

class EncoderDriver:

    def __init__(self, IN1_pin, IN2_pin, timer,PPR=4000):
        '''
        @brief Creates an encoder object.
        @param In1_pin The 1st pyb.Pin object that is specific to the desired encoder. Input must be a string!!
        @param In2_pin The 2nd pyb.Pin object that is specific to the desired encoder. Input must be a string!!
        @param timer The timer specific to the physical encoder
        @param PPR Encoder Pulses per Revolution (PPR); used to compute position in degrees.
        '''
        tim = pyb.Timer(timer)
        tim.init(prescaler=0, period=0xFFFF)
        pin_IN1 = pyb.Pin(IN1_pin)
        pin_IN2 = pyb.Pin(IN2_pin)
        tim.channel(1, pin=pin_IN1, mode=pyb.Timer.ENC_AB)
        tim.channel(2, pin=pin_IN2, mode=pyb.Timer.ENC_AB)

        self.pos = array.array('I',[0, 0]) # Creates buffer for current and previous positions
        self.rel_pos = 0
        self.deg_pos = 0
        self.tim = tim      # creates timer object
        
        self.bad_delta = 0
        self.good_delta = 0
        
        self.PPR = PPR
            
    def update(self):
        '''
        @brief Updates the encoder's position and records encoder's previous position
        '''   
        self.pos[-2] = self.pos[-1]
        self.pos[-1] = self.tim.counter()
        self.bad_delta = self.pos[-1] - self.pos[-2]
        
        self.rel_pos = self.rel_pos + self.get_delta()
        
        self.deg_pos = 360 * (self.rel_pos / self.PPR)
        self.deg_delta = 360 * (self.good_delta / self.PPR)
        
    def get_delta(self):
        '''
        @brief Computes the difference between the encoder's current position and previous position
        '''
        if -0x7FFF < self.bad_delta < 0x7FFF:
            self.good_delta = self.bad_delta
        elif self.bad_delta < -0x7FFF:
            self.good_delta = self.bad_delta + 0xFFFF
        elif self.bad_delta > 0x7FFF:
            self.good_delta = self.bad_delta - 0xFFFF
        else:
            pass
        return int(self.good_delta)
        
    def get_position(self):
        '''
        @brief Prints the encoder's current position
        '''
        return int(self.deg_pos)
    
    def set_position(self, pos):
        '''
        @brief Sets the encoder's position to a given input
        '''
        self.rel_pos = pos
        return self.rel_pos
    
    def zero(self):
        '''
        @brief Zeros out the encoder
        '''
        self.rel_pos = 0
        return self.rel_pos

    
if __name__ == '__main__':
    encx = EncoderDriver('PB6','PB7',4)
    ency = EncoderDriver('PC6','PC7',8)
    
    while True:
        try:
            t1 = utime.ticks_us()
            encx.update()
            ency.update()
            t2 = utime.ticks_us()
            readtime = utime.ticks_diff(t2,t1)
            
            line = 'Encoder X: {px:.2f} --> {dx:.2f}     Encoder Y: {py:.2f} --> {dy:.2f}'
            
            print('\n'*50)
            print(line.format(px=encx.deg_pos,dx=encx.deg_delta,py=ency.deg_pos,dy=ency.deg_delta))
            print('Readtime: ' + str(readtime))
            utime.sleep_ms(500)
            
        except KeyboardInterrupt:
            break