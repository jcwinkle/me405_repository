%% ME 405 Lab 0x06: Simulation of Reality?
clc;
clear;
close all;
clear vars;

%% Constants

r_m = 60e-3;               % radius of lever arm [m]
l_r = 50e-3;               % length of push rod [m]
r_B = 10.5e-3;             % radius of ball [m]
r_G = 42e-3;               % Vertical Distance from U-joint to platform CG [m]
l_P = 110e-3;              % Horizontal Distance from U-joint to push-rod pivot [m]
r_P = 32.5e-3;             % Vertical Distance from U-joint to push-rod pivot [m]
r_C = 50e-3;               % Vertical Distance from U-joint to platform surface [m]
m_B = 30e-3;               % Mass of Ball [kg]
m_P = 400e-3;              % Mass of Platform [kg]
I_P = 1.88e-3;             % Moment of Inertia of Platform [kg*m^2]
I_B = (2/5)*m_B*r_B^2;     % Moment of Inertia of Ball [kg*m^2]
b = 10e-3;                 % Viscous friction at U-joint [N*m *(s/rad)]
g = 9.81;                  % acceleration due to gravity [m/s^2]

%% Open-Loop State Space Model
% the below matrix terms were created when linearizing the lab 5 
% derivation. the linearization was done at the equilibrium point, assuming
% x_o = u_o = 0. 

A = -((m_B*r_B^2) + (m_B*r_B*r_C) + I_B)/r_B;
C = -((m_B*r_B^2) +  I_B)/r_B;
D = -((m_B*r_B^2) + (m_B*r_B*r_C) + I_B);
H = (I_B + I_P + (m_B*r_B^2) + (2*m_B*r_B*r_C) + (m_B*r_C^2) + (m_P*r_G^2));
I = A*D + C*H;
J = -(g*m_B*(r_B + r_C)^2) - g*m_P*r_G - g*m_B*r_B;
K = -g*m_B*r_B;

% State space matrices: 
% X_DOT = AA*X + BB*u
% Y = CC*X + DD*u
% X_DOT = [x_dot, theta_dot, x_doubledot, theta_doubledot]
% X = [x, theta, x_dot, theta_dot]
% u = [T_m]

AA = [   0          0        1    0;
         0          0        0    1;
    -g*m_B*D/I  (D*J+H*K)/I  0  D*b/I;
     g*m_B*C/I  (A*K-C*J)/I  0  -C*b/I];
  
BB = [      0;
            0;
     (D*l_P)/(I*r_m);
    -(C*l_P)/(I*r_m)];

CC = eye(4);
DD = zeros(4, 1);

out1 = sim('lab06_simulink.slx'); 

% %% Open-loop Case 1 Plot:
% % The ball is initially at rest on a level platform directly above the 
% % center of gravity of the platform and there is no torque input from the 
% % motor.
% figure(1);
% subplot(4,1,1);
% plot(out1.tout, out1.X1(:,1));
% title('x vs. time');
% xlabel('time [s]');
% ylabel('x [mm]');
% subplot(4,1,2);
% plot(out1.tout, out1.X1(:,2));
% title('theta vs. time');
% xlabel('time [s]');
% ylabel('theta [rad]');
% subplot(4,1,3);
% plot(out1.tout, out1.X1(:,3));
% title('xdot vs. time');
% xlabel('time [s]');
% ylabel('x [mm/s]');
% subplot(4,1,4);
% plot(out1.tout, out1.X1(:,4));
% title('thetadot vs. time');
% xlabel('time [s]');
% ylabel('theta [rad/s]');
% 
% %% Open-loop Case 2 Plot:
% % The  ball  is  initially  at  rest  on  a  level  platform  offset  
% % horizontally  from  the  center  ofgravity of the platform by 5 [cm] and 
% % there is no torque input from the motor.
% figure(2);
% subplot(4,1,1);
% plot(out1.tout, out1.X2(:,1));
% title('x vs. time');
% xlabel('time [s]');
% ylabel('x [m]');
% subplot(4,1,2);
% plot(out1.tout, out1.X2(:,2));
% title('theta vs. time');
% xlabel('time [s]');
% ylabel('theta [rad]');
% subplot(4,1,3);
% plot(out1.tout, out1.X2(:,3));
% title('xdot vs. time');
% xlabel('time [s]');
% ylabel('xdot [m/s]');
% subplot(4,1,4);
% plot(out1.tout, out1.X2(:,4));
% title('thetadot vs. time');
% xlabel('time [s]');
% ylabel('thetadot [rad/s]');
% 
% %% Open-loop Case 3 Plot:
% % The ball is initially at rest on a platform inclined at 5◦ 
% % directly above the center of gravityof the platform and there is no 
% % torque input from the motor. 
% figure(3);
% subplot(4,1,1);
% plot(out1.tout, out1.X3(:,1));
% title('x vs. time');
% xlabel('time [s]');
% ylabel('x [m]');
% subplot(4,1,2);
% plot(out1.tout, out1.X3(:,2));
% title('theta vs. time');
% xlabel('time [s]');
% ylabel('theta [rad]');
% subplot(4,1,3);
% plot(out1.tout, out1.X3(:,3));
% title('xdot vs. time');
% xlabel('time [s]');
% ylabel('xdot [m/s]');
% subplot(4,1,4);
% plot(out1.tout, out1.X3(:,4));
% title('thetadot vs. time');
% xlabel('time [s]');
% ylabel('thetadot [rad/s]');
% 
% %% Open-loop Case 4 Plot:
% % The ball is initially at rest on a level platform directly above the 
% % center of gravity ofthe platform and there is an impulse of 1 mNm·s 
% % applied by the motor.  
% figure(4);
% subplot(4,1,1);
% plot(out1.tout, out1.X4(:,1));
% title('x vs. time');
% xlabel('time [s]');
% ylabel('x [m]');
% subplot(4,1,2);
% plot(out1.tout, out1.X4(:,2));
% title('theta vs. time');
% xlabel('time [s]');
% ylabel('theta [rad]');
% subplot(4,1,3);
% plot(out1.tout, out1.X4(:,3));
% title('xdot vs. time');
% xlabel('time [s]');
% ylabel('xdot [m/s]');
% subplot(4,1,4);
% plot(out1.tout, out1.X4(:,4));
% title('thetadot vs. time');
% xlabel('time [s]');
% ylabel('thetadot [rad/s]');

%% Controlled (Closed-loop) State Space Model
% Now we define the controlled system by adding in a feedback loop such that
% u = -K_cont*X, where K_cont is given below:

K_cont = [-13.86     -5.33     -3.27    -0.3];
         % [N]    [N*m]     [N*s]   [N*m*s]
         
out2 = sim('lab06_simulink2.slx');

% Redoing Case 2 for controlled system (5 cm initial offset):
figure(5);
subplot(4,1,1);
plot(out2.tout, out2.X2(:,1));
title('x vs. time');
xlabel('time [s]');
ylabel('x [m]');
subplot(4,1,2);
plot(out2.tout, out2.X2(:,2));
title('theta vs. time');
xlabel('time [s]');
ylabel('theta [rad]');
subplot(4,1,3);
plot(out2.tout, out2.X2(:,3));
title('xdot vs. time');
xlabel('time [s]');
ylabel('xdot [m/s]');
subplot(4,1,4);
plot(out2.tout, out2.X2(:,4));
title('thetadot vs. time');
xlabel('time [s]');
ylabel('thetadot [rad/s]');





