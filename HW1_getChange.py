'''
@file HW1_getChange.py
@brief File defining the function getChange for ME 405 HW 1
@details Contains the function, getChange, along with test code. 
@author Jacob Winkler
@date January 11, 2021
'''

def getChange(price, payment):
    '''
    @brief Computes change for monetary transaction
    @details Computes change given set of bills/coins and returns minimum 
             amount of denominations as change
    @param price An integer number of cents representing the price of an item
    @param payment An 8-element tuple representing the integer number of 
                   pennies, nickels, dimes, quarters, ones, fives, tens, and 
                   twenties, in that order, paid to purchase an item
    @return if payment value is sufficient, returns a tuple of bills/coins.
            if payment is insufficient, returns None. 
    '''
    val_pennies = 1*payment[0]
    val_nickels = 5*payment[1]
    val_dimes = 10*payment[2]
    val_quarters = 25*payment[3]
    val_ones = 100*payment[4]
    val_fives = 500*payment[5]
    val_tens = 1000*payment[6]
    val_twenties = 2000*payment[7]
    payment_value = val_pennies + val_nickels + val_dimes + val_quarters + val_ones + val_fives + val_tens + val_twenties
    change = payment_value - price
    if change < 0:
        return None
    elif change >= 0:
        vals = [2000, 1000, 500, 100, 25, 10, 5, 1]
        change_list = []
        for i in vals:
            if change - i >= 0:
                x = change - change%i
                y = x/i
                change_list.append(int(y))
                change = change - x
            elif change - i < 0:
                change_list.append(0)
            else:
                pass
        change_list.reverse()
        #change_val = payment_value - price
        return change_list
      
if __name__ == '__main__':
    item_cost = 1000
    my_money = (0, 0, 0, 0, 0, 0, 1, 0)
    my_change = getChange(item_cost, my_money)
    print(my_change)
