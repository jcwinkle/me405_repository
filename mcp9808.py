'''
@file mcp9808.py
@brief File containing the mcp9808 class 
@details The mcp9808 class contained in this file allows the user to 
         communicate with an MCP9808 temperature sensor using its I2C 
         interface. It can verify that the sensor is attached at the given bus 
         address as well as return the ambiant room temperature in either 
         Celcius or Farenheit.
@author Jacob Winkler
@date February 3, 2021
'''

from pyb import I2C
import utime
    
class mcp9808:
    '''
    @class mcp9808
    @brief Driver class for the mcp9808 temperature collector. See \ref mcp9808.py for details.
    '''
    def __init__(self, address):
        '''
        @brief Initiates the class intiating I2C communication and storing a copy of desired mcp9808 address 
        @param address The Device address for the connected mcp9808
        '''
        ## Creates I2C on Bus 1 and initiates it as Master
        self.i2c = I2C(1, I2C.MASTER)
        self.address = address
        
       
    def check(self):
        '''
        @brief Checks whether the sensor is attached at the given bus address
        @return Boolean True or False 
        '''   
        ## 16-bit Buffer Array to write from Manufacturer ID Register
        check_buffer = bytearray(2)
        
        # Write into checkbuf from manufacturer ID register
        self.i2c.mem_read(check_buffer, addr = self.address, memaddr = 6)
        
        # Check if Published Manufacturer ID matches received ID
        if check_buffer[1] == 0x0054:
            return True
        else:
            return False
        
    def celcius(self):
        '''
        @brief Returns the ambiant room temperature read by the mcp9808 in Degrees Celsius
        @return A float value for the temperature read in Degrees Celsius.
        '''
        ## Buffer Array to write Temperature Data Into
        Cel_buf = bytearray(2)
        
        # Write Temperature Data to buf from corresponding register on MCP9808
        self.i2c.mem_read(Cel_buf, addr = self.address, memaddr = 5)
        
        # Clear Flags
        Cel_buf[0] = Cel_buf[0] & 0x1F
        # Check if data is below 0*C
        if Cel_buf[0] & 0x10 == 0x10:
            # If data is below 0*C, clear sign and "resign" in integer calc
            Cel_buf[0] = Cel_buf[0] & 0x0F
            tempCelcius = (Cel_buf[0]*16+Cel_buf[1]/16)-256   # Converts to temperature in decimal
        else:    
            # Runs if data indicates above 0*C
            tempCelcius = Cel_buf[0]*16+Cel_buf[1]/16      # Converts to temperature in decimal
        return tempCelcius
    
    def farenheit(self):
        '''
        @brief Returns the ambiant room temperature read by the mcp9808 in Degrees Farenheit.
        @return A float value for the temperature read in Degrees Farenheit.
        '''
        Faren_buf = bytearray(2)
        self.i2c.mem_read(Faren_buf, addr = self.address, memaddr = 5)
        Faren_buf[0] = Faren_buf[0] & 0x1F
        # Check if data is below 0*C
        if Faren_buf[0] & 0x10 == 0x10:
            # If data is below 0*C, clear sign and "resign" in integer calc
            Faren_buf[0] = Faren_buf[0] & 0x0F
            tempCelcius = (Faren_buf[0]*16+Faren_buf[1]/16)-256
            
        else:    
            # Runs if data indicates above 0*C
            tempCelcius = Faren_buf[0]*16+Faren_buf[1]/16
        tempFaren = tempCelcius*(9/5)+32
        return tempFaren
    
    
if __name__ == '__main__':
    #This function returns the temperature in degrees celsius every second (if the driver is running correctly)
    mcp = mcp9808(24)
    #Check that Manufacturer ID matches published.
    mcp.check()
    while True:
        print(mcp.celsius())
        utime.sleep(1)
            